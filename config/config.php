<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

// Local
$app['locale'] = 'en';
$app['session.default_locale'] = $app['locale'];
$app['translator.messages'] = [
    'en' => PATH_LOCAL . '/en.yml',
];

// disable the debug mode by default
$app['debug'] = false;
$app['debug.visual'] = false;

// Cache
$app['cache.path'] = PATH_CACHE;

// Http cache
$app['http_cache.cache_dir'] = $app['cache.path'] . '/http';

//Log
$app['monolog.config'] = [
    'debug_log_file' => PATH_LOG . '/debug.log' ,
    'error_log_file' => PATH_LOG . '/error.log' ,
];

// Twig templates
$app['twig.config'] = [
    'cache' => PATH_CACHE . '/twig',
    'template_path' => PATH_TEMPLATES
];

// Profiler
$app['profiler.config'] = [
    'cache_dir' => PATH_CACHE . '/profiler',
    'debug_toolbar' => true,
    'debug_redirects' => false
];

// Doctrine (dbs)
//It can be overridden in environment config file.
$app['dbs.config'] = [
    'resource_1' => [
        'driver' => 'pdo_mysql',
        'charset'  => 'utf8',
        'host'     => '127.0.0.1',
        'dbname'   => 'resource_1_database_name',
        'user'     => 'root',
        'password' => 'root'
    ],
    'resource_2' => [
        'driver' => 'pdo_mysql',
        'charset'  => 'utf8',
        'host'     => '127.0.0.1',
        'dbname'   => 'resource_2_database_name',
        'user'     => 'root',
        'password' => 'root'
    ]
];

//OAuth parameters
$app['oauth.config'] = [
    'storage' => 'pdo',
    'connection' => [
        'dsn' => 'mysql:dbname=oauth;host=localhost',
        'user' => 'root',
        'password' => 'root'
    ]
];

$app['xrr.config.default_output'] = '1.3.1';
$app['xrr.config.default_input'] = '2.0.1';
$app['xrr.config.1.3.0'] = '<xrrconfig>
	<category name="xrr_root_tag_attr">
		<setting name="xmlns:xsi" value="http://www.w3.org/2001/XMLSchema-instance" />
		<setting name="xsi:noNamespaceSchemaLocation" value="http://www.sailing.org/techpages/resources/xml/sailingXRR_v1.3.0.xsd" />
		<setting name="Version" value="1.3.0" />
		<setting name="Type" value="RegattaResult" />
	</category>
</xrrconfig>';
$app['xrr.config.1.3.1'] = '<xrrconfig>
	<category name="xrr_root_tag_attr">
		<setting name="xmlns:xsi" value="http://www.w3.org/2001/XMLSchema-instance" />
		<setting name="xsi:noNamespaceSchemaLocation" value="http://www.sailing.org/techpages/resources/xml/sailingXRR_v1.3.1.xsd" />
		<setting name="Version" value="1.3.1" />
		<setting name="Type" value="RegattaResult" />
	</category>
</xrrconfig>';
$app['xrr.config.2.0.1'] = '<xrrconfig>
	<category name="xrr_root_tag_attr">
		<setting name="xmlns:xsi" value="http://www.w3.org/2001/XMLSchema-instance" />
		<setting name="xsi:noNamespaceSchemaLocation" value="http://www.sailing.org/techpages/resources/xml/sailingXRR_v2.0.1.xsd" />
		<setting name="Version" value="2.0.1" />
		<setting name="Type" value="RegattaResult" />
	</category>
</xrrconfig>';
