<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model;

use \ReflectionClass;

/**
 * Class AbstractEnum
 * @package Model
 */
abstract class AbstractEnum {
    /**
     * @var null
     */
    protected static $constCacheArray = NULL;

    /**
     * First elemint of every value is return value of toXrr()
     * @var string[]
     */
    protected static $valueMap;

    /**
     * @param $name
     * @param bool|false $strict
     * @return bool
     */
    public static function isValidName($name, $strict = false)
    {
        $constants = self::getConstants();

        if ($strict) {
            return array_key_exists($name, $constants);
        }

        $keys = array_map('strtoupper', array_keys($constants));
        return in_array(strtoupper($name), $keys);
    }

    /**
     * @return mixed
     */
    public static function getConstants()
    {

        if (self::$constCacheArray == NULL) {
            self::$constCacheArray = [];
        }

        $calledClass = get_called_class();
        if (!array_key_exists($calledClass, self::$constCacheArray)) {
            $reflect = new ReflectionClass($calledClass);
            self::$constCacheArray[$calledClass] = $reflect->getConstants();
        }

        return self::$constCacheArray[$calledClass];
    }

    /**
     * @param $value
     * @param bool|true $strict
     * @return bool
     */
    public static function isValidValue($value, $strict = true)
    {
        $values = array_values(self::getConstants());
        return in_array($value, $values, $strict);
    }

    /**
     * Value coming from XRR, we map that to a real value.
     * Value don't have to be a valid constant on this static, but uppercase(value) must be declared in valueMap. This function converts value to a valid constant.
     * @param $value
     * @param bool $strict
     * @return string
     * @throws \Exception
     */
    public static function fromXrr($value, $strict = true)
    {
        if (self::isValidValue($value)) {
            return $value;
        } else if (self::isValidValue($value, false)) {
            return strtoupper($value);
        } else {
            if (isset(static::$valueMap[strtoupper($value)])) {
                return static::$valueMap[strtoupper($value)];
            } else {
                if ($strict === true) {
                    $calledClass = get_called_class();
                    throw new \Exception('Invalid enum map key [' . $calledClass . '][' . $value . ']');
                } else {
                    return $value;
                }
            }
        }
    }

    /**
     * Value coming from DB, we map that to the preferred XRR value, which is the first key with asked value in valueMap.
     * Value must be a valid constant on this static!
     * @param $value
     * @param bool $strict
     * @return string
     * @throws \Exception
     */
    public static function toXrr($value, $strict = true)
    {
        $value = self::fromXrr($value);

        if (count(static::$valueMap) > 0) {
            $key = array_search($value, static::$valueMap, true);
            if ($key !== false) {
                return $key;
            } else {
                if ($strict === true) {
                    $calledClass = get_called_class();
                    throw new \Exception('Invalid enum map value [' . $calledClass . '][' . $value . ']');
                } else {
                    return $value;
                }
            }
        } else {
            return $value;
        }

    }

    /**
     * @return array
     */
    public static function toArray()
    {
        return array_values(static::getConstants());
    }
}
