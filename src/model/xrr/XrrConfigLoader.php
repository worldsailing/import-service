<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace Model\xrr;

use Silex\Application;
use \DOMDocument;
use Model\xrr\common\enum\XrrVersions;
use worldsailing\Helper\WsHelper;

class XrrConfigLoader
{
    /**
     * @param Application $app
     * @param string|null $data
     * @return array
     * @throws \Exception
     */
    public static function setConfigSet(Application $app, $data = null)
    {
        $config = ['input' => [], 'output' => []];
        $version = $app['xrr.config.default_output'];
        if (! isset($app['xrr.config.' . XrrVersions::fromXrr($version)])) {
            throw new \Exception('Undeclared XRR version [xrr.config.default_output][' . 'xrr.config.' . XrrVersions::fromXrr($version) . ']' );
        }

        $config['output'] = self::loadConfig($app, XrrVersions::fromXrr($version));

        if ($data !== null ) {
            $version = false;
            $dom = new DOMDocument();
            try {
                if ($dom->loadXML($data)) {
                    $docelem = $dom->documentElement;
                    if (strtoupper($docelem->tagName) !== "ODFBODY" && $docelem->hasAttribute("Version")) {
                        $version = XrrVersions::fromXrr($dom->documentElement->getAttribute("Version"));
                    }
                }
            } catch (\Exception $e) {
                $app['monolog']->error('Error at reading xml document', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
                throw new \Exception($e->getMessage(), 400);
            }
            if (isset($dom)) {
                unset($dom);
            }
            if (! isset($app['xrr.config.' . XrrVersions::fromXrr($version)])) {
                throw new \Exception('Unsupported XRR version [' . XrrVersions::fromXrr($version) . ']', 400);
            }
            $config['input'] = self::loadConfig($app, XrrVersions::fromXrr($version));
        } else {
            $version = $app['xrr.config.default_input'];
            if (! isset($app['xrr.config.' . XrrVersions::fromXrr($version)])) {
                throw new \Exception('Undeclared XRR version [xrr.config.default_input][' . 'xrr.config.' . XrrVersions::fromXrr($version) . ']', 500);
            }
            $config['input'] = self::loadConfig($app, XrrVersions::fromXrr($version));
        }
        return $config;
    }

    /**
     * @param Application $app
     * @param $version
     * @return array
     */
    private static function loadConfig(Application $app, $version)
    {
        $config = [];
        $xmlConfig = $app['xrr.config.' . $version];
        $configDom = new DOMDocument();
        $configDom->loadXML($xmlConfig);
        $categories = $configDom->getElementsByTagName("category");
        for($cidx = 0; $cidx < $categories->length; $cidx++)
        {
            $catgelem = $categories->item($cidx);
            $catgname = $catgelem->getAttribute("name");
            if(!isset($config[$catgname]))
            {
                $config[$catgname] = array();
            }

            $settings = $catgelem->getElementsByTagName("setting");
            for($sidx = 0; $sidx < $settings->length; $sidx++)
            {
                $setting = $settings->item($sidx);
                $config[$catgname][$setting->getAttribute("name")] = $setting->getAttribute("value");
            }
        }
        return $config;
    }
}
