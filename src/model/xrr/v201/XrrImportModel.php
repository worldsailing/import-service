<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace Model\xrr\v201;

use Model\AbstractModel;
use Model\xrr\v201\enum\XrrEventClass;
use Model\xrr\v201\enum\XrrEventGender;
use Model\xrr\v201\enum\XrrEventGenderExt;
use Model\xrr\XrrImportModelInterface;
use Doctrine\Common\Collections\Criteria;
use Model\xrr\common\enum\XrrComparisonPatterns;
use worldsailing\Api\response\ErrorEntity;
use worldsailing\Api\response\ErrorList;

class XrrImportModel extends AbstractModel implements XrrImportModelInterface
{
    /**
     * @var bool
     */
    protected $simulate = false;
    /**
     * @var
     */
    protected $data;
    /**
     * @var array
     */
    protected $sailorSourceIds = [];
    /**
     * @var array
     */
    protected $boatSourceIds = [];
    /**
     * @var array
     */
    protected $teamSourceIds = [];
    /**
     * @var array
     */
    protected $regattas = [];
    /**
     * @var array
     */
    protected $tournaments = [];
    /**
     * @var array
     */
    protected $teams = [];

    /**
     * @return \worldsailing\api\response\ErrorEntity[]
     */
    protected $validationMessages = [];
    /**
     * @var bool
     */
    protected $checkSailorNames = false;
    /**
     * @var string
     */
    protected $nameComparisonCharCode = XrrComparisonPatterns::UTF8;


    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     * @return XrrImportModel
     */
    public function loadData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return array
     */
    public function getRegattas()
    {
        return $this->regattas;
    }

    /**
     * @param array $regattas
     * @return XrrImportModel
     */
    public function setRegattas($regattas)
    {
        $this->regattas = $regattas;
        return $this;
    }

    /**
     * @return array
     */
    public function getTournaments()
    {
        return $this->tournaments;
    }

    /**
     * @param array $tournaments
     * @return XrrImportModel
     */
    public function setTournaments($tournaments)
    {
        $this->tournaments = $tournaments;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTeams()
    {
        return $this->teams;
    }

    /**
     * @param mixed $teams
     * @return XrrImportModel
     */
    public function setTeams($teams)
    {
        $this->teams = $teams;
        return $this;
    }

    /**
     * @param mixed $key
     * @param $team
     * @return $this
     */
    public function addTeam($key, $team)
    {
        $this->teams[$key] = $team;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSimulate()
    {
        return $this->simulate;
    }

    /**
     * @param bool $simulate
     * @return XrrImportModel
     */
    public function setSimulate($simulate)
    {
        $this->simulate = $simulate;
        return $this;
    }


    /**
     * @return \worldsailing\api\response\ErrorEntity[]
     */
    public function getValidationMessages()
    {
        return $this->validationMessages;
    }

    /**
     * @param \worldsailing\api\response\ErrorEntity[]|\worldsailing\api\response\ErrorList $validationMessages
     * @return XrrImportModel
     */
    public function setValidationMessages($validationMessages)
    {
        if ($validationMessages instanceof ErrorList) {
            $errors = $validationMessages->map();
            foreach ($errors as $error) {
                $this->addValidationMessages(new ErrorEntity($error));
            }
        } else {
            $this->validationMessages = $validationMessages;
        }
        return $this;
    }

    /**
     * @param array|string $validationMessage
     * @param array|string|int|null $code
     * @return XrrImportModelInterface
     */
    public function addValidationMessages($validationMessage, $code = null)
    {
        if (is_array($validationMessage)) {
            foreach ($validationMessage as $k => $message) {
                if ($code && is_array($code)) {
                    $this->addValidationMessages($message, (isset($code[$k]) ? $code[$k] : ''));
                } elseif ($code) {
                    $this->addValidationMessages($message, $code);
                } else {
                    $this->addValidationMessages($message, '');
                }
            }
        } else {
            $this->validationMessages[] = new ErrorEntity('error', [
                'type' => 'validation_error',
                'code' => ($code && (! is_array($code))) ? $code : '',
                'message' => $validationMessage,
                'context' => ''
            ]);
        }
        return $this;
    }

    /**
     * @return bool
     */
    public function isCheckSailorNames()
    {
        return $this->checkSailorNames;
    }

    /**
     * @param bool $checkSailorNames
     */
    public function setCheckSailorNames($checkSailorNames)
    {
        $this->checkSailorNames = $checkSailorNames;
    }

    /**
     * @return string
     */
    public function getNameComparisonCharCode()
    {
        return $this->nameComparisonCharCode;
    }

    /**
     * @param string $nameComparisonCharCode
     */
    public function setNameComparisonCharCode($nameComparisonCharCode)
    {
        $this->nameComparisonCharCode = $nameComparisonCharCode;
    }

    /**
     * @return array
     */
    public function getSailorSourceIds()
    {
        return $this->sailorSourceIds;
    }

    /**
     * @param array $sailorSourceIds
     * @return XrrImportModel
     */
    public function setSailorSourceIds($sailorSourceIds)
    {
        $this->sailorSourceIds = $sailorSourceIds;
        return $this;
    }

    /**
     * @param $id
     * @return $this
     */
    public function addSailorSourceId($id)
    {
        $this->sailorSourceIds[] = $id;
        return $this;
    }

    /**
     * @return array
     */
    public function getBoatSourceIds()
    {
        return $this->boatSourceIds;
    }

    /**
     * @param array $boatSourceIds
     * @return XrrImportModel
     */
    public function setBoatSourceIds($boatSourceIds)
    {
        $this->boatSourceIds = $boatSourceIds;
        return $this;
    }

    /**
     * @param $id
     * @return $this
     */
    public function addBoatSourceId($id)
    {
        $this->boatSourceIds[] = $id;
        return $this;
    }

    /**
     * @return array
     */
    public function getTeamSourceIds()
    {
        return $this->teamSourceIds;
    }

    /**
     * @param array $teamSourceIds
     * @return XrrImportModel
     */
    public function setTeamSourceIds($teamSourceIds)
    {
        $this->teamSourceIds = $teamSourceIds;
        return $this;
    }

    /**
     * @param $id
     * @return $this
     */
    public function addTeamSourceId($id)
    {
        $this->teamSourceIds[] = $id;
        return $this;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        return (count($this->validationMessages) === 0) ? true : false;
    }


    /**
     * @param $version
     * @return bool|mixed
     */
    public function getSchema($version)
    {
        $criteria = new Criteria();
        $criteria->where($criteria::expr()->eq('XRRSchDocVersion', $version));
        $criteria->andWhere($criteria::expr()->eq('XRRSchDocType', 'SCHEMA'));

        /** @var \Doctrine\Common\Collections\Collection $result */
        $result = $this->app['orm.ems']['xrr']->getRepository('worldsailing\Isaf\model\XrrSchemaDoc')->matching($criteria);
        if (count($result) > 1) {
            $this->app['monolog']->warning('More than one schema descriptor detected [' . $version . ']');
        }
        return (count($result) >0 ) ? $result[0] : false;
    }

    /**
     * @param array $isafIds
     * @param bool $reArrange
     * @return bool|\worldsailing\Isaf\model\MemberBiogs[]
     */
    public function getBiogs($isafIds = [], $reArrange = false)
    {

        $criteria = new Criteria();
        $criteria->where($criteria::expr()->in('BiogIsafId', $isafIds));
        $criteria->andWhere($criteria::expr()->eq('BiogDeleted', 'no'));
        $model = $this->app['service.simpleBiog']->createBiogModel($this->app);

        /** @var \worldsailing\Common\BundleResultSet\CompositeListResultSet $result */
        $biogs = $this->app['service.simpleBiog']->getSimpleBiogListByCriteria($model, $criteria);
        if ($biogs->isData()) {
            if ($reArrange === true) {
                $result = [];
                foreach($biogs->getData() as $biog) {
                    $result[$biog->getBiogIsafId()] = $biog;
                }
                return $result;
            } else {
                return $biogs->getData();
            }
        } else {
            return false;
        }
    }

    /**
     * @param $id
     * @return bool|\worldsailing\Isaf\model\Regattas
     */
    public function getRegattaById($id)
    {
        $criteria = new Criteria();
        $criteria->where($criteria::expr()->eq('RgtaId', $id));
        $criteria->andWhere($criteria::expr()->eq('RgtaApvlStatus', 'Y'));

        /** @var \Doctrine\Common\Collections\Collection $result */
        $result = $this->app['orm.ems']['regatta']->getRepository('worldsailing\Isaf\model\Regattas')->matching($criteria);
        return (count($result)) ? $result[0] : false;
    }

    /**
     * @param $isafId
     * @return bool|\worldsailing\Isaf\model\Regattas
     */
    public function getRegattaByIsafId($isafId)
    {
        $criteria = new Criteria();
        $criteria->where($criteria::expr()->eq('RgtaIsafId', $isafId));
        $criteria->andWhere($criteria::expr()->eq('RgtaApvlStatus', 'Y'));

        /** @var \Doctrine\Common\Collections\Collection $result */
        $result = $this->app['orm.ems']['regatta']->getRepository('worldsailing\Isaf\model\Regattas')->matching($criteria);
        return (count($result)) ? $result[0] : false;
    }

    /**
     * @param $id
     * @return bool|\worldsailing\Isaf\model\Tournaments
     */
    public function getTournamentById($id)
    {
        $criteria = new Criteria();
        $criteria->where($criteria::expr()->eq('TournId', $id));

        /** @var \Doctrine\Common\Collections\Collection $result */
        $result = $this->app['orm.ems']['regatta']->getRepository('worldsailing\Isaf\model\Tournaments')->matching($criteria);
        return (count($result)) ? $result[0] : false;
    }

    /**
     * @param $classId
     * @return bool|\worldsailing\Isaf\model\Classes
     */
    public function getClassByClassId($classId)
    {
        $criteria = new Criteria();
        $criteria->where($criteria::expr()->eq('ClassId', $classId));

        /** @var \Doctrine\Common\Collections\Collection $result */
        $result = $this->app['orm.ems']['regatta']->getRepository('worldsailing\Isaf\model\Classes')->matching($criteria);
        return (count($result)) ? $result[0] : false;
    }


    /**
     * @param $id
     * @param $rgtaId
     * @return bool|\worldsailing\Isaf\model\Events
     */
    public function getEventByIdAndRgtaId($id, $rgtaId)
    {
        $criteria = new Criteria();
        $criteria->where($criteria::expr()->eq('EvntId', $id));
        $criteria->andWhere($criteria::expr()->eq('EvntRgtaId', $rgtaId));

        /** @var \Doctrine\Common\Collections\Collection $result */
        $result = $this->app['orm.ems']['regatta']->getRepository('worldsailing\Isaf\model\Events')->matching($criteria);
        return (count($result)) ? $result[0] : false;
    }

    /**
     * @param mixed $regattaId
     * @param string $isafClassCode
     * @param string $genderCode
     * @param bool $isDisabled
     * @return array|bool
     */
    public function identifyEvent($regattaId, $isafClassCode, $genderCode, $isDisabled = false)
    {
        if(!is_numeric($regattaId)) {
            $regatta = $this->getRegattaByIsafId($regattaId);
            if($regatta) {
                $regattaId = $regatta->getRgtaId();
            }
        }
        $ret = null;
        if(!is_numeric($genderCode)) {
            $genderCode = XrrEventGender::fromXrr($genderCode);
        }
        if($isDisabled) {
            $genderCode = XrrEventGenderExt::DISABLED;
        }

        $isafClassCode = XrrEventClass::fromXrr($isafClassCode);

        $sql = "SELECT Events.*, EventTypes.TypeName AS EvntTypeName FROM Events 
                    JOIN EventTypes ON EventTypes. TypeId = Events.EvntTypeId
                    JOIN Classes ON Classes.ClassId = Events.EvntClassId
                  WHERE EventTypes.TypeId = ? AND Classes.ClassCode = ? AND Events.EvntRgtaId = ? 
                 ";

        $result = $this->app['dbs']['isaf']->fetchAssoc($sql, [$genderCode, $isafClassCode, $regattaId]);

        return ($result) ? $result : false;
    }

    /**
     * @param \worldsailing\Isaf\model\MemberBiogs[] $biogsResult
     * @return array
     */
    public function reArrangeMembers($biogsResult)
    {
        $members = [];
        foreach($biogsResult as $member) {
            $members[$member->getBiogIsafId()] = $member;
        }
        return $members;
    }


    /**
     * @param array $isafIds
     * @param bool $reArrange
     * @return array|bool
     */
    public function getMembers($isafIds = [], $reArrange = false)
    {
        $model =  $this->app['service.soticMembers']->createMembersModel($this->app);
        $members = $this->app['service.soticMembers']->getActiveMembersByIsafId($model, $isafIds);
        if ($members->isData()) {
            if ($reArrange === true) {
                $result = [];
                foreach($members->getData() as $member) {
                    $result[$member['MembLogin']] = $member;
                }
                return $result;
            } else {
                return $members->getData();
            }
        } else {
            return false;
        }
    }

}

