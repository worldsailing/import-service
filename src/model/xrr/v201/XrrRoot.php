<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v201;

use \DOMDocument;
use Model\xrr\common\enum\XrrRootDocTypes;
use Model\xrr\v201\element\XrrSailingXrr;

/**
 * Class XrrRoot
 * @package Model\xrr\v201
 */
class XrrRoot extends AbstractXrrModel
{
    /**
     * @var array
     */
    private $validation_errors;

    public function __construct($data = null)
    {
        if($data)
        {
            /*
            $dom = null;
            try
            {
                $dom = new DOMDocument("1.0", "UTF-8");
                libxml_use_internal_errors(true);
                if(!$dom->loadXML($data))
                {
                    $this->validation_errors = libxml_get_errors();
                    foreach($this->validation_errors as $eidx => $error)
                    {
                        $this->validation_errors[$eidx] = $error->message . " line: " . $error->line;
                    }
                }
                libxml_use_internal_errors(false);

                $root = $dom->documentElement;
                $newelem = $dom->createElement("root");

                $newelem->appendChild($root);
                $data = $newelem->ownerDocument->saveXML($newelem);
                $dom = new DOMDocument("1.0", "UTF-8");
                $dom->loadXML($data);
                $data = $dom->saveXML();
            }
            catch(\Exception $ex){
            }

            if($dom)
            {
                parent::__construct($data);
            }
            */
            parent::__construct($data);
        }

    }

    protected function ListInternElements()
    {
        return XrrRootDocTypes::ToArray();
    }

    public function Document()
    {

        $elements = array();
        foreach($this->ListInternElements() as $tagname)
        {

            /** @var \DOMElement[] $elements */
            $elements = $this->Elements($tagname);
            if(count($elements) > 0)
            {
                break;
            }
        }

        if(count($elements)) {
            return new XrrSailingXrr($this->data);
        } else {
            throw new \Exception('XRR root is empty');
        }
    }

    protected function Elements($tagname)
    {
        if ($this->data) {
            $classname = "Model\\xrr\\v201\\element\\Xrr" . $this->TransformClassName($tagname);
            $this->el[$tagname][] = $this->data;//$this->data->getElementById('SailingXrr');// new $classname($this->xmlDoc);
        }
        return isset($this->el[$tagname]) ? $this->el[$tagname] : array();
    }
}
