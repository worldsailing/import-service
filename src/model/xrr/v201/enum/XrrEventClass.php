<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v201\enum;

use Model\AbstractEnum;

/**
 * Class XrrEventClass
 * @package Model\xrr\v201\enum
 */
class XrrEventClass extends AbstractEnum
{

    const _470 = "470";
    const _49ER = "49ER";
    const _2_4MR = "2.4MR";
    const _FINN = "FINN";
    const _LSR = "LSR";
    const _LAR = "LAR";
    const _RSX = "RSX";
    const _SKUD18 = "SKUD18";
    const _SONAR = "SONAR";
    const _STR = "STR";
    const _N17 = "N17";
    const _49FX = "49FX";
    const _E6M = "E6M";

    /**
     * @var array
     */
    protected static $valueMap = [
        "470" => self::_470,
        "49ER" => self::_49ER,
        "2.4MR" => self::_2_4MR,
        "FINN" => self::_FINN,
        "LSR" => self::_LSR,
        "LAR" => self::_LAR,
        "RSX" => self::_RSX,
        "SKUD18" => self::_SKUD18,
        "SONAR" => self::_SONAR,
        "STR" => self::_STR,
        "N17" => self::_N17,
        "49FX" => self::_49FX,
        "E6M" => self::_E6M,
    ];
}
