<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v201\enum;

use Model\AbstractEnum;

/**
 * Class XrrReleaseLevel
 * @package Model\xrr\v201\enum
 */
class XrrReleaseLevel extends AbstractEnum
{
    const COURSES = "Courses";
    const WEATHER = "Weather";
    const MARKRESULTS = "Mark Results";
    const RACERESULTS = "Race Results";
    const SERIESRESULTS = "Series Results";
    const DOCUMENTS = "Documents";
    const GPX = "GPX";

    /**
     * @var array
     */
    protected static $valueMap = [
        "COURSES" => self::COURSES,

        "WEATHER" => self::WEATHER,

        "MARK_RESULTS" => self::MARKRESULTS,
        "MARKRESULTS" => self::MARKRESULTS,
        "MARK RESULTS" => self::MARKRESULTS,

        "RACE_RESULTS" => self::RACERESULTS,
        "RACERESULTS" => self::RACERESULTS,
        "RACE RESULTS" => self::RACERESULTS,

        "SERIES_RESULTS" => self::SERIESRESULTS,
        "SERIESRESULTS" => self::SERIESRESULTS,
        "SERIES RESULTS" => self::SERIESRESULTS,

        "DOCUMENTS" => self::DOCUMENTS,

        "GPX" => self::GPX
    ];
}
