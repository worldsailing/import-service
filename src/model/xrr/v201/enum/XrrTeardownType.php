<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v201\enum;

use Model\AbstractEnum;

/**
 * Class XrrTeardownType
 * @package Model\xrr\v201\enum
 */
class XrrTeardownType extends AbstractEnum
{
    /**
     *
     */
    const REGATTA = "Regatta";
    /**
     *
     */
    const EVENT = "Event";

    /**
     * @var array
     */
    protected static $valueMap = [
        "REGATTA" => self::REGATTA,
        "R" => self::REGATTA,

        "EVENT" => self::EVENT,
        "E" => self::EVENT
    ];
}
