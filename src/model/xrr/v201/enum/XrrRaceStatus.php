<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v201\enum;

use Model\AbstractEnum;

/**
 * Class XrrRaceStatus
 * @package Model\xrr\v201\enum
 */
class XrrRaceStatus extends AbstractEnum
{
    const PLANNED = 'planned';
    const SCHEDULED = 'scheduled';
    const RESCHEDULED = 'rescheduled';
    const STARTSEQ = 'startseq';
    const INPROGRESS = 'inprogress';
    const RECALLED = 'recall';
    const POSTPONED = 'postponed';
    const ABANDONED = 'abandoned';
    const FINISHED = 'finished';
    const PROVISIONAL = 'provisional';
    const CANCELLED = 'cancelled';
    const _FINAL = 'final';

    /**
     * @var array
     */
    protected static $valueMap = [
        "PLANNED" => self::PLANNED,
        "SCHEDULED" => self::SCHEDULED,
        "RESCHEDULED" => self::RESCHEDULED,
        "STARTSEQ" => self::STARTSEQ,
        "INPROGRESS" => self::INPROGRESS,
        "RECALLED" => self::RECALLED,
        "POSTPONED" => self::POSTPONED,
        "ABANDONED" => self::ABANDONED,
        "FINISHED" => self::FINISHED,
        "PROVISIONAL" => self::PROVISIONAL,
        "CANCELLED" => self::CANCELLED,
        "_FINAL" => self::_FINAL,
        "FINAL" => self::_FINAL
    ];
}
