<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v201\enum;

use Model\AbstractEnum;

/**
 * Class XrrAction
 * @package Model\xrr\v201\enum
 */
class XrrAction extends AbstractEnum
{
    /**
     *
     */
    const AUTO_UPLOAD = "AUTO_UPLOAD";
    /**
     *
     */
    const TREE_POPULATE = "TREE_POPULATE";

    /**
     * @var array
     */
    protected static $valueMap = [
        "AUTO_UPLOAD" => self::AUTO_UPLOAD,

        "TREE_POPULATE" => self::TREE_POPULATE
    ];
}
