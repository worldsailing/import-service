<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v201\enum;

use Model\AbstractEnum;

/**
 * Class XrrEventType
 * @package Model\xrr\v201\enum
 */
class XrrEventType extends AbstractEnum
{
    /**
     *
     */
    const REGATTA = "IndividualRegatta";
    /**
     *
     */
    const REGATTA_SERIES = "RegattaSeries";

    /**
     * @var array
     */
    protected static $valueMap = [
        "INDIVIDUAL_REGATTA" => self::REGATTA,
        "INDIVIDUALREGATTA" => self::REGATTA,
        "REGATTA" => self::REGATTA,

        "REAGATTA_SERIES" => self::REGATTA_SERIES,
        "REGATTASERIES" => self::REGATTA_SERIES,
        "SERIES" => self::REGATTA_SERIES,
        "TRNMNT" => self::REGATTA_SERIES
    ];
}
