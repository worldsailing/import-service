<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v201\enum;

use Model\AbstractEnum;

/**
 * Class XrrClassificationStatus
 * @package Model\xrr\v201\enum
 */
class XrrClassificationStatus extends AbstractEnum
{
    const CURRENT = "Current";
    const PENDING = "Pending";
    const EXPIRED = "Expired";
    const NONE = "None";

    /**
     * @var array
     */
    protected static $valueMap = [
        "CURRENT" => self::CURRENT,
        "PENDING" => self::PENDING,
        "EXPIRED" => self::EXPIRED,
        "NONE" => self::NONE
    ];
}
