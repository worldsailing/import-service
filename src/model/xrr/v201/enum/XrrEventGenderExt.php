<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v201\enum;

use Model\AbstractEnum;

/**
 * Class XrrEventGender
 * @package Model\xrr\v201\enum
 */
class XrrEventGenderExt extends AbstractEnum
{
    const OPEN = "1";
    const MALE = "2";
    const FEMALE = "3";
    const YOUTH = "4";
    const DISABLED = "5";
    const MIXED = "6";

    /**
     * @var array
     */
    protected static $valueMap = array(
        "MALE" => self::MALE,
        "MAN" => self::MALE,
        "MEN" => self::MALE,
        "M" => self::MALE,

        "FEMALE" => self::FEMALE,
        "WOMAN" => self::FEMALE,
        "WOMEN" => self::FEMALE,
        "W" => self::FEMALE,
        "F" => self::FEMALE,

        "MIXED" => self::MIXED,
        "X" => self::MIXED,

        "OPEN" => self::OPEN,
        "OPENED" => self::OPEN,
        "O" => self::OPEN,

        "YOUTH" => self::YOUTH,
        "JUNIOR" => self::YOUTH,
        "Y" => self::YOUTH,

        "DISABLED" => self::DISABLED,
        "D" => self::DISABLED
    );

}
