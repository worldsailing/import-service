<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v201\element;

use Model\xrr\v201\AbstractXrrModel;

/**
 * Class XrrCompoundMark
 * @package Model\xrr\v201\element
 */
class XrrCompoundMark extends AbstractXrrModel
{
    protected function ListInternElements()
    {
        return array("Mark", "Other");
    }

    public function CompoundMarkID($value = null)
    {
        return $this->Attribute("CompoundMarkID", $value);
    }

    public function Name($value = null)
    {
        return $this->Attribute("Name", $value);
    }

    public function Marks()
    {
        return $this->Elements("Mark");
    }

    public function Other()
    {
        return $this->Elements("Other");
    }
}
