<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v201\element;

use Model\xrr\v201\AbstractXrrModel;
use Model\xrr\v201\enum\XrrRaceStatus;
use Helper\XrrHelper;

/**
 * Class XrrRace
 * @package Model\xrr\v201\element
 */
class XrrRace extends AbstractXrrModel
{

    public function StartScheduled($value = null)
    {
        $ret = $this->_StartScheduled($value);
        if (!$ret) {
            $date = $this->StartDate();
            $time = $this->StartTime();
            if ($date && $time) {
                $ret = implode("T", array($date, $time));
            }
        }
        return $ret;
    }

    public function _StartScheduled($value = null)
    {
        return $this->Attribute("StartScheduled", $value);
    }

    public function StartDate($value = null)
    {
        return XrrHelper::legacyDateParse($this->Attribute("RaceStartDate", $value));
    }

    public function StartTime($value = null)
    {
        return XrrHelper::legacyTimeParse($this->Attribute("RaceStartTime", $value));
    }

    public function StartActual($value = null)
    {
        $ret = $this->_StartActual($value);
        if (!$ret && $this->RaceHasStarted()) {
            $date = $this->StartDate();
            $time = $this->StartTime();
            if ($date && $time) {
                $ret = implode("T", array($date, $time));
            }
        }
        return $ret;
    }

    public function _StartActual($value = null)
    {
        return $this->Attribute("StartActual", $value);
    }

    public function RaceHasStarted()
    {
        return in_array($this->RaceStatus(), array(XrrRaceStatus::FINISHED, XrrRaceStatus::INPROGRESS, XrrRaceStatus::PROVISIONAL, XrrRaceStatus::STARTSEQ));
    }

    public function RaceStatus($value = null)
    {
        return $this->Attribute("RaceStatus", $value);
    }

    public function RaceId($value = null)
    {
        return strtoupper($this->Attribute("RaceID", $value));
    }

    public function RaceName($value = null)
    {
        return $this->Attribute("RaceName", $value);
    }

    public function RaceNumber($value = null)
    {
        return $this->Attribute("RaceNumber", $value);
    }

    public function Stage($value = null)
    {
        return $this->Attribute("Stage", $value);
    }

    public function Flight($value = null)
    {
        return $this->Attribute("Flight", $value);
    }

    public function Match($value = null)
    {
        return $this->Attribute("Match", $value);
    }

    public function Course()
    {
        $ret = $this->Elements("Course");
        if (count($ret) > 0) {
            $ret = $ret[0];
        } else {
            $ret = null;
        }
        return $ret;
    }

    public function RankingDivisionId()
    {
        $ret = null;
        $others = $this->Other();
        foreach ($others as $other) {
            /* @var $other XrrOther */
            if ($other->Name() === "RankingDivisionId") {
                $ret = $other->Value();
                break;
            }
        }
        return $ret;
    }

    public function Other()
    {
        return $this->Elements("Other");
    }

    protected function ListInternElements()
    {
        return array("Other", "Course");
    }
}
