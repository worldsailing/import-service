<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v201\element;

use Model\xrr\v201\AbstractXrrModel;

/**
 * Class XrrMark
 * @package Model\xrr\v201\element
 */
class XrrMark extends AbstractXrrModel
{
    protected function ListInternElements()
    {
        return array("Other");
    }

    public function SeqID($value = null)
    {
        return $this->Attribute("SeqID", $value);
    }

    public function Name($value = null)
    {
        return $this->Attribute("Name", $value);
    }

    public function Other()
    {
        return $this->Elements("Other");
    }

}
