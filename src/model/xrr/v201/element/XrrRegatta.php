<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v201\element;

use Model\xrr\v201\AbstractXrrModel;
use Model\xrr\v201\enum\XrrEventType;

/**
 * Class XrrRegatta
 * @package Model\xrr\v201\element
 */
class XrrRegatta extends AbstractXrrModel
{
    protected function ListInternElements()
    {
        return array("Race", "Division", "RegattaSeriesResult", "Other");
    }

    public function Races()
    {
        return $this->Elements("Race");
    }

    public function Divisions()
    {
        return $this->Elements("Division");
    }

    public function RegattaSeriesResults()
    {
        return $this->Elements("RegattaSeriesResult");
    }

    public function Other()
    {
        return $this->Elements("Other");
    }

    public function RegattaId($value = null)
    {
        return strtoupper($this->Attribute("RegattaID", $value));
    }

    public function IsafRegattaId($value = null)
    {
        return strtoupper($this->Attribute("IFRegattaID", $value));
    }

    public function IsafUploadCode($value = null)
    {
        return $this->Attribute("IFUploadCode", $value);
    }

    public function EventTitle($value = null)
    {
        return $this->Attribute("Title", $value);
    }

    public function EventType($value = null)
    {
        if($value !== null)
        {
            $value = XrrEventType::toXrr($value);
        }
        return $this->Attribute("Type", $value);
    }

}
