<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace Model\xrr\v201\element;

use Model\xrr\v201\AbstractXrrModel;

/**
 * Class XrrParticipant
 * @package Model\xrr\v201\element
 */
class XrrParticipant extends AbstractXrrModel
{
    protected function ListInternElements()
    {
        return array("Other");
    }

    public function PersonId($value = null)
    {
        return strtoupper($this->Attribute("PersonID", $value));
    }

    public function Comment($value = null)
    {
        return $this->Attribute("Comment", $value);
    }
}
