<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v201\element;

use Model\xrr\v201\AbstractXrrModel;

/**
 * Class XrrContactInfo
 * @package Model\xrr\v201\element
 */
class XrrContactInfo extends AbstractXrrModel
{
    protected function ListInternElements()
    {
        return array("Other");
    }

    public function Other()
    {
        return $this->Elements("Other");
    }

    public function AddressLine1($value = null)
    {
        return $this->Attribute("AddressLine1", $value);
    }

    public function AddressLine2($value = null)
    {
        return $this->Attribute("AddressLine2", $value);
    }

    public function AddressLine3($value = null)
    {
        return $this->Attribute("AddressLine3", $value);
    }

    public function City($value = null)
    {
        return $this->Attribute("City", $value);
    }

    public function Province($value = null)
    {
        return $this->Attribute("Province", $value);
    }

    public function PostalCode($value = null)
    {
        return $this->Attribute("PostalCode", $value);
    }

    public function Country($value = null)
    {
        return $this->Attribute("Country", $value);
    }

    public function DayPhone($value = null)
    {
        return $this->Attribute("DayPhone", $value);
    }

    public function EveningPhone($value = null)
    {
        return $this->Attribute("EveningPhone", $value);
    }

    public function MobilePhone($value = null)
    {
        return $this->Attribute("MobilePhone", $value);
    }

    public function Fax($value = null)
    {
        return $this->Attribute("Fax", $value);
    }

    public function Email($value = null)
    {
        return $this->Attribute("Email", $value);
    }

}
