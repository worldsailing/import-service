<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v201\element;

use Model\xrr\v201\AbstractXrrModel;

/**
 * Class XrrMarkSequence
 * @package Model\xrr\v201\element
 */
class XrrMarkSequence extends AbstractXrrModel
{
    protected function ListInternElements()
    {
        return array("Corner", "Other");
    }

    public function Corners()
    {
        return $this->Elements("Corner");
    }

    public function Other()
    {
        return $this->Elements("Other");
    }

}
