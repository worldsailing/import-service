<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v201\element;

use Model\xrr\v201\AbstractXrrModel;

/**
 * Class XrrMarkRounding
 * @package Model\xrr\v201\element
 */
class XrrMarkRounding extends AbstractXrrModel
{
    protected function ListInternElements()
    {

    }

    public function SeqId($value = null)
    {
        return $this->Attribute("SeqId", $value);
    }

    public function Position($value = null)
    {
        return $this->Attribute("Position", $value);
    }

    public function Time($value = null)
    {
        return $this->Attribute("Time", $value);
    }

}
