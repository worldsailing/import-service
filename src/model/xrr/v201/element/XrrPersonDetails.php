<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v201\element;

use Model\xrr\v201\AbstractXrrModel;

/**
 * Class XrrPersondetails
 * @package Model\xrr\v201\element
 */
class XrrPersondetails extends AbstractXrrModel
{
    protected function ListInternElements()
    {
        return array("PrimaryContactInfo", "AlternateContactInfo", "Image", "Other");
    }

    public function Hometown($value = null)
    {
        return $this->Attribute("Hometown", $value);
    }

    public function DateOfBirth($value = null)
    {
        return $this->Attribute("DateOfBirth", $value);
    }

    public function PlaceOfBirth($value = null)
    {
        return $this->Attribute("PlaceOfBirth", $value);
    }

    public function MNAMemberNumber($value = null)
    {
        return $this->Attribute("MNAMemberNumber", $value);
    }

    public function MaritalStatus($value = null)
    {
        return $this->Attribute("MaritalStatus", $value);
    }

    public function Children($value = null)
    {
        return $this->Attribute("Children", $value);
    }

    public function Occupation($value = null)
    {
        return $this->Attribute("Occupation", $value);
    }

    public function YachtClub($value = null)
    {
        return $this->Attribute("YachtClub", $value);
    }

    public function YachtClubLocation($value = null)
    {
        return $this->Attribute("YachtClubLocation", $value);
    }

    public function Coach($value = null)
    {
        return $this->Attribute("Coach", $value);
    }

    public function CoachedSince($value = null)
    {
        return $this->Attribute("CoachedSince", $value);
    }

    public function Education($value = null)
    {
        return $this->Attribute("Education", $value);
    }

    public function CampaignWebsite($value = null)
    {
        return $this->Attribute("CampaignWebsite", $value);
    }

    public function BoatFirstSailed($value = null)
    {
        return $this->Attribute("BoatFirstSailed", $value);
    }

    public function StartedSailingAge($value = null)
    {
        return $this->Attribute("StartedSailingAge", $value);
    }

    public function CurrentSailingClass($value = null)
    {
        return $this->Attribute("CurrentSailingClass", $value);
    }

    public function PreviousSailingClass($value = null)
    {
        return $this->Attribute("PreviousSailingClass", $value);
    }

    public function FirstEventWon($value = null)
    {
        return $this->Attribute("FirstEventWon", $value);
    }

    public function Interests($value = null)
    {
        return $this->Attribute("Interests", $value);
    }

    /**
     * @return XrrContactInfo|null
     */
    public function PrimaryContactInfo()
    {
        $ret = null;
        $elems = $this->Elements("PrimaryContactInfo");
        if(count($elems) > 0)
        {
            $ret = $elems[0];
        }
        /* @var $ret XrrContactInfo */
        return $ret;
    }

    /**
     * @return XrrContactInfo|null
     */
    public function AlternateContactInfo()
    {
        $ret = null;
        $elems = $this->Elements("AlternateContactInfo");
        if(count($elems) > 0)
        {
            $ret = $elems[0];
        }
        /* @var $ret XrrContactInfo */
        return $ret;
    }

    public function Image()
    {
        return $this->Elements("Image");
    }

    public function SpokenLanguages()
    {
        $ret = array();
        $elems = $this->Elements("SpokenLanguage");
        foreach($elems as $langelem)
        {
            $ret[] = $langelem->nodeValue;
        }
        return $ret;
    }

    public function Other()
    {
        return $this->Elements("Other");
    }
}
