<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v201\element;

use Model\xrr\v201\AbstractXrrModel;

/**
 * Class XrrCorner
 * @package Model\xrr\v201\element
 */
class XrrCorner extends AbstractXrrModel
{

    protected function ListInternElements()
    {
        return array("Other");
    }

    public function SeqId($value = null)
    {
        return $this->Attribute("SeqId", $value);
    }

    public function CompoundMarkID($value = null)
    {
        return $this->Attribute("CompoundMarkID", $value);
    }

    public function Rounding($value = null)
    {
        return $this->Attribute("Rounding", $value);
    }

    public function ZoneSize($value = null)
    {
        return $this->Attribute("ZoneSize", $value);
    }

    public function Other()
    {
        return $this->Elements("Other");
    }
}
