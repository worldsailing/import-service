<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v201\element;

use Model\xrr\v201\AbstractXrrModel;
use Model\xrr\v201\enum\XrrMatchRaceEntry;
use Helper\XrrHelper;

/**
 * Class XrrRaceResult
 * @package Model\xrr\v201\element
 */
class XrrRaceResult extends AbstractXrrModel
{

    protected function ListInternElements()
    {
        return array("Crew", "Other", "MarkRounding");
    }

    public function Crew()
    {
        return $this->Elements("Crew");
    }

    public function Other()
    {
        return $this->Elements("Other");
    }

    public function RaceId($value = null)
    {
        return strtoupper($this->Attribute("RaceID", $value));
    }

    public function TeamId($value = null)
    {
        return strtoupper($this->Attribute("TeamID", $value));
    }

    public function BoatId($value = null)
    {
        return strtoupper($this->Attribute("BoatID", $value));
    }

    public function RacePoints($value = null)
    {
        return $this->Attribute("RacePoints", $value);
    }

    public function RaceRank($value = null)
    {
        return $this->Attribute("RaceRank", $value);
    }

    private static $codemap = array("RTD" => "RET");

    public function ScoreCode($value = null)
    {
        if (isset(self::$codemap[$value])) {
            $value = self::$codemap[$value];
        }
        $ret = $this->Attribute("ScoreCode", $value);
        if (isset(self::$codemap[$ret])) {
            $ret = self::$codemap[$ret];
        }
        return $ret;
    }

    public function Entry($value = null)
    {
        if ($value !== null) {
            $value = XrrMatchRaceEntry::toXrr($value);
        }
        return XrrMatchRaceEntry::toXrr($this->Attribute("Entry", $value));
    }

    public function Discard($value = null)
    {
        if ($value !== null) {
            $value = XrrHelper::boolIn($value);
        }
        return XrrHelper::boolOut($this->Attribute("Discard", $value));
    }

    public function MarkRoundings()
    {
        return $this->Elements("MarkRounding");
    }
}
