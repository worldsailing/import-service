<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v201\element;
/**
 * Class XrrRegattaResult
 * @package Model\xrr\v201\element
 */
class XrrRegattaResult extends XrrSailingXrr
{


}
