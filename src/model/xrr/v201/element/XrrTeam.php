<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v201\element;

use Model\xrr\v201\AbstractXrrModel;
use Model\xrr\v201\enum\XrrEventGender;

/**
 * Class XrrTeam
 * @package Model\xrr\v201\element
 */
class XrrTeam extends AbstractXrrModel
{
    protected function ListInternElements()
    {
        return array("Crew", "Other");
    }

    public function Crew()
    {
        return $this->Elements("Crew");
    }

    public function Other()
    {
        return $this->Elements("Other");
    }

    public function IOCNationCode($value = null)
    {
        return $this->Attribute("NOC", $value);
    }

    public function BoatId($value = null)
    {
        return strtoupper($this->Attribute("BoatID", $value));
    }

    public function TeamId($value = null)
    {
        return strtoupper($this->Attribute("TeamID", $value));
    }

    public function TeamName($value = null)
    {
        return $this->Attribute("TeamName", $value);
    }

    public function TeamGender($value = null)
    {
        if($value !== null)
        {
            $value = XrrEventGender::toXrr($value);
        }
        return XrrEventGender::toXrr($this->Attribute("Gender", $value));
    }

}
