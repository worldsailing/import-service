<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v201\element;

use Model\xrr\v201\AbstractXrrModel;

/**
 * Class XrrCourse
 * @package Model\xrr\v201\element
 */
class XrrCourse extends AbstractXrrModel
{
    protected function ListInternElements()
    {
        return array("CompoundMark", "MarkSequence", "Other");
    }

    public function CourseID($value = null)
    {
        return $this->Attribute("CourseID", $value);
    }

    public function IFCourseID($value = null)
    {
        return $this->Attribute("IFCourseID", $value);
    }

    public function RaceId($value = null)
    {
        return $this->Attribute("RaceID", $value);
    }

    public function CourseCode($value = null)
    {
        return $this->Attribute("CourseCode", $value);
    }

    public function CourseName($value = null)
    {
        return $this->Attribute("CourseName", $value);
    }

    public function IFRaceId($value = null)
    {
        return $this->Attribute("IFRaceID", $value);
    }

    public function CompoundMarks()
    {
        return $this->Elements("CompoundMark");
    }

    public function MarkSequence()
    {
        $ret = null;
        $elems = $this->Elements("MarkSequence");
        if(count($elems) > 0)
        {
            $ret = $elems[0];
        }
        return $ret;
    }

    public function Other()
    {
        return $this->Elements("Other");
    }

}
