<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace Model\xrr\v201\element;

use Model\xrr\v201\AbstractXrrModel;

class XrrBoat extends AbstractXrrModel
{
    protected function ListInternElements()
    {
        return array("Other");
    }

    public function Other()
    {
        return $this->Elements("Other");
    }

    public function BoatId($value = null)
    {
        return strtoupper($this->Attribute("BoatID", $value));
    }

    public function BoatName($value = null)
    {
        return $this->Attribute("BoatName", $value);
    }

    public function SailNumber($value = null)
    {
        return $this->Attribute("SailNumber", $value);
    }

    public function IFBoatId($value = null)
    {
        return $this->attribute("IFBoatID", $value);
    }
}
