<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace Model\xrr\v201\element;


use Model\xrr\v201\AbstractXrrModel;
use Model\xrr\v201\enum\XrrDivisionType;
use Model\xrr\v201\enum\XrrEventGender;
use Helper\XrrHelper;

class XrrDivision extends AbstractXrrModel
{
    protected function ListInternElements()
    {
        return array("RaceResult", "SeriesResult", "TRResult", "Other");
    }

    public function RaceResults()
    {
        return $this->Elements("RaceResult");
    }

    public function SeriesResults()
    {
        return $this->Elements("SeriesResult");
    }

    public function TeamRaceResults()
    {
        return $this->Elements("TRResult");
    }

    public function Other()
    {
        return $this->Elements("Other");
    }

    public function DivisionId($value = null)
    {
        return strtoupper($this->Attribute("DivisionID", $value));
    }

    public function ISAFDivisionId($value = null)
    {
        return strtoupper($this->Attribute("IFDivisionID", $value));
    }

    public function Title($value = null)
    {
        return $this->Attribute("Title", $value);
    }

    public function DivisionType($value = null)
    {
        return $this->Attribute("Type", $value);
    }

    public function IFClassCode($value = null)
    {
        return $this->Attribute("IFClassID", $value);
    }

    public function Gender($value = null)
    {
        return $this->Attribute("Gender", $value);
    }

    public function IsDisabled($value = null)
    {
        return $this->Attribute("Disabled", $value);
    }

    public function ParentDivision($value = null)
    {
        return $this->Attribute("ParentDivision");
    }

    public function StartingDivision($value = null)
    {
        return $this->Attribute("StartingDivision", $value);
    }

    public function RankingDivision($value = null)
    {
        return $this->Attribute("RankingDivision", $value);
    }
}
