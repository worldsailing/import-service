<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace Model\xrr\v201;

use \DOMDocument;
use \DOMElement;
use Model\xrr\v201\element\XrrSailingXrr;
use worldsailing\Helper\WsHelper;

/**
 * Class AbstractXrrModel
 * @package Model\xrr\v201
 */
abstract class AbstractXrrModel
{
    /**
     * @var string
     */
    protected $data;
    /**
     * @var \DOMDocument
     */
    protected $xmlDoc;
    protected $value;
    protected $at = array();
    protected $el = array();
    protected $rawTagName;
    protected static $genDoc;
    private static $timestamp;
    /**
     * @var AbstractXrrModel
     */
    protected $parentElement = null;

    public function __construct($data = null)
    {
        self::$timestamp = time();
        if ($data) {
            $this->DataParse($data);
        }
    }

    /**
     * @param $data
     * @throws \Exception
     */
    protected function DataParse($data)
    {
        $this->data = $data;
        $this->xmlDoc = new DOMDocument('1.0', 'UTF-8');
        try {
            libxml_use_internal_errors(true);
            $this->xmlDoc->resolveExternals = TRUE;
            $this->xmlDoc->loadXML($this->data);
            libxml_use_internal_errors(false);

            //$this->rawTagName = $this->xmlDoc->documentElement->nodeName;

            $this->Attributes();

            $this->Populate();
        } catch(\Exception $e) {
            throw new \Exception($e->getMessage(), 400);
        }
        //$this->data = null;
        //$this->xmlDoc = null;
    }

    /**
     *
     */
    protected function Populate()
    {
        $elements = $this->ListInternElements();
        if (is_array($elements)) {
            foreach ($elements as $tagname) {
                $this->Elements($tagname);
            }
        }
        $this->value = $this->xmlDoc->nodeValue;
    }

    /**
     * Produce a list of element names to be contained in the current element
     *
     * @return array<string> The element name list
     */
    protected abstract function ListInternElements();

    protected function Attributes()
    {
        if ($this->xmlDoc) {
            $node = $this->xmlDoc->documentElement;
            if ($node->hasAttributes()) {
                foreach ($node->attributes as $key => $attrib) {
                    $this->at[$attrib->nodeName] = $attrib->nodeValue;
                }
            }

            if ($this instanceof XrrSailingXrr) {
                $simplexml = simplexml_import_dom($this->xmlDoc);
                if ($simplexml) {
                    $namespaces = $simplexml->getNamespaces(false);
                    foreach ($namespaces as $pseudonym => $location) {
                        $this->at["xmlns:" . $pseudonym] = $location;
                    }
                }
            }
        }
    }

    protected function Attribute($name, $value = null)
    {
        if ($value !== null) {
            $this->at[$name] = $value;
        }
        return isset($this->at[$name]) ? $this->at[$name] : null;
    }

    protected function Elements($tagname)
    {
        if (!isset($this->el[$tagname])) {
            $this->el[$tagname] = array();

            $element = $this->xmlDoc;
            if ($element instanceof DOMDocument) {
                $element = $this->xmlDoc->documentElement;
            }
            $list = $this->GetImmediateChildrenByTagName($element, $tagname);
            $listlength = count($list);
            $classname = "Model\\xrr\\v201\\element\\Xrr" . $this->TransformClassName($tagname);

            for ($lidx = 0; $lidx < $listlength; $lidx++) {

                if (class_exists($classname)) {
                    $this->el[$tagname][] = (new $classname($this->xmlDoc->saveXML($list[$lidx])))->setParent($this);
                } else {
                    $this->el[$tagname][] = $list[$lidx];
                }
            }
        }
        return isset($this->el[$tagname]) ? $this->el[$tagname] : array();
    }

    protected function GetImmediateChildrenByTagName($element, $tagName)
    {
        $result = array();
        if ($element instanceof DOMElement) {
            foreach ($element->childNodes as $child) {
                if ($child instanceof DOMElement && $child->tagName == $tagName) {
                    $result[] = $child;
                }
            }
        }
        return $result;
    }

    public function AddElement($element)
    {
        if (is_object($element)) {
            $class = get_class($element);
            if (strpos($class, "XRR_") === 0) {
                $tagname = str_replace("XRR_", "", $class);
                if (!isset($this->el[$tagname])) {
                    $this->el[$tagname] = array();
                }
                $this->el[$tagname][] = $element;
            }
        }
    }

    protected function TagName()
    {
        $className = WsHelper::getClassShortName($this);
        if (WsHelper::getClassShortName($this) === "XrrBuilder") {
            $className = "SailingXRR";
        }

        return $className;
    }


    public function ToXML()
    {
        $appendtoroot = false;
        if (!self::$genDoc) {
            self::$genDoc = new DOMDocument("1.0", "UTF-8");
            $appendtoroot = true;
        }

        $thiselement = self::$genDoc->createElement($this->rawTagName, $this->value);
        if ($appendtoroot) {
            self::$genDoc->appendChild($thiselement);
        }
        /** TODO: XRR export
         * if($this instanceof XRRBuilder && count($this->at) === 0)
         * {
         * $this->at = DataFeedProtocolPlugin::Configuration("xrr_root_tag_attr");
         * $this->at['DateTime'] = $this->XRRDateTime();
         * }
         */
        foreach ($this->at as $name => $value) {
            $thiselement->setAttribute($name, $value);
        }

        if (count($this->el) > 0) {
            foreach ($this->el as $name => $element_list) {
                foreach ($element_list as $element) {
                    $thiselement->appendChild($element->ToXML($thiselement));
                }
            }
        }
        $ret = null;

        /** TODO: XRR export
         * if(in_array($this->RawTagName(), XrrRootDocTypes::toArray()) || $this instanceof XRRBuilder)
         * {
         * $ret = self::$genDoc->saveXML(self::$genDoc);
         * }
         *
         * else
         * {
         * $ret = $thiselement;
         * }
         */
        $ret = $thiselement;
        return $ret;
    }


    public function RawTagName()
    {
        return $this->rawTagName;
    }

    protected function TransformClassName($tagname)
    {
        $ret = $tagname;
        switch ($tagname) {
            case "PrimaryContactInfo":
            case "AlternateContactInfo":
                if ($this->rawTagName === "PersonDetails")
                    $ret = "ContactInfo";
                break;
            case "Image":
                if ($this->rawTagName === "PersonDetails") {
                    $ret = "PersonImage";
                }
                break;
        }
        return $ret;
    }

    protected function getTextFromNode(\DOMElement $element)
    {
        $newdoc = new DOMDocument();
        $cloned = $element->cloneNode(TRUE);
        $newdoc->appendChild($newdoc->importNode($cloned,TRUE));
        return $newdoc->saveHTML();
    }

    public function setParent($parent)
    {
        $this->parentElement = $parent;
        return $this;
    }

    public function getParent()
    {
        return $this->parentElement;
    }
}
