<?php

/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace Model\xrr\v131;

use Silex\Application;
use \DOMDocument;
use \DOMElement;
use Model\AbstractModel;
use Model\xrr\v131\element\SailingXrr;
use Model\xrr\common\enum\XrrRootDocTypes;
use Helper\XrrHelper;

/**
 * Class AbstractXrrModel
 * @package Model\xrr\v131
 */
abstract class AbstractXrrModel extends AbstractModel
{
    /**
     * @var string
     */
    protected $data;
    /**
     * @var \DOMDocument
     */
    protected $xmlDoc;
    protected $value;
    protected $at = array();
    protected $el = array();
    protected $rawTagName;
    private static $timestamp;

    public function __construct(Application $app, $data = null)
    {
        parent::__construct($app);
        self::$timestamp = time();
        if ($data) {
            $this->DataParse($data);
        }
    }

    /**
     * @param $data
     */
    protected function DataParse($data)
    {
        $this->data = $data;
        $this->xmlDoc = new DOMDocument();
        $this->xmlDoc->loadXML($this->data);
        $this->rawTagName = $this->xmlDoc->documentElement->nodeName;
        $this->Attributes();
        $this->Populate();
        $this->data = null;
        $this->xmlDoc = null;
    }

    /**
     *
     */
    protected function Populate()
    {
        $elements = $this->ListInternElements();
        if (is_array($elements)) {
            foreach ($elements as $tagname) {
                $this->Elements($tagname);
            }
        }
        $this->value = $this->xmlDoc->nodeValue;
    }

    /**
     * Produce a list of element names to be contained in the current element
     *
     * @return array<string> The element name list
     */
    protected abstract function ListInternElements();

    private function Attributes()
    {
        if ($this->xmlDoc) {
            $node = $this->xmlDoc->documentElement;

            foreach ($node->attributes as $attrib) {
                $this->at[$attrib->nodeName] = $attrib->nodeValue;
            }
            if ($this instanceof SailingXrr) {
                $simplexml = simplexml_import_dom($this->xmlDoc);
                if ($simplexml) {
                    $namespaces = $simplexml->getNamespaces(false);
                    foreach ($namespaces as $pseudonym => $location) {
                        $this->at["xmlns:" . $pseudonym] = $location;
                    }
                }
            }
        }
    }

    protected function Attribute($name, $value = null)
    {
        if ($value !== null) {
            $this->at[$name] = $value;
        }
        return isset($this->at[$name]) ? $this->at[$name] : null;
    }

    protected function Elements($tagname)
    {
        if (!isset($this->el[$tagname])) {
            $this->el[$tagname] = array();

            $element = $this->xmlDoc;
            if ($element instanceof DOMDocument) {
                $element = $this->xmlDoc->documentElement;
            }

            $list = $this->GetImmediateChildrenByTagName($element, $tagname);
            $listlength = count($list);
            $classname = "XRR_" . $this->TransformClassName($tagname);

            for ($lidx = 0; $lidx < $listlength; $lidx++) {
                if (class_exists($classname)) {
                    $this->el[$tagname][] = new $classname($this->xmlDoc->saveXML($list[$lidx]));
                } else {
                    $this->el[$tagname][] = $list[$lidx];
                }
            }
        }
        return isset($this->el[$tagname]) ? $this->el[$tagname] : array();
    }

    private function GetImmediateChildrenByTagName($element, $tagName)
    {
        $result = array();
        if ($element instanceof DOMElement) {
            foreach ($element->childNodes as $child) {
                if ($child instanceof DOMElement && $child->tagName == $tagName) {
                    $result[] = $child;
                }
            }
        }
        return $result;
    }

    public function AddElement($element)
    {
        if (is_object($element)) {
            $class = get_class($element);
            if (strpos($class, "XRR_") === 0) {
                $tagname = str_replace("XRR_", "", $class);
                if (!isset($this->el[$tagname])) {
                    $this->el[$tagname] = array();
                }
                $this->el[$tagname][] = $element;
            }
        }
    }

    protected function TagName()
    {
        $classname = array();
        if (get_class($this) === "XRRBuilder") {
            $classname = array("XRR_", "SailingXRR");
        } else {
            $classname = explode("XRR_", get_class($this));
        }

        $ret = null;
        if (isset($classname[1])) {
            $ret = $classname[1];
        }
        return $ret;
    }

    protected static $gendoc;

    public function ToXML()
    {
        $appendtoroot = false;
        if (!self::$gendoc) {
            self::$gendoc = new DOMDocument("1.0", "UTF-8");
            $appendtoroot = true;
        }

        $thiselement = self::$gendoc->createElement($this->rawTagName, $this->value);
        if ($appendtoroot) {
            self::$gendoc->appendChild($thiselement);
        }
        /** TODO: XRR export
         * if($this instanceof XRRBuilder && count($this->at) === 0)
         * {
         * $this->at = DataFeedProtocolPlugin::Configuration("xrr_root_tag_attr");
         * $this->at['DateTime'] = $this->XRRDateTime();
         * }
         */
        foreach ($this->at as $name => $value) {
            $thiselement->setAttribute($name, $value);
        }

        if (count($this->el) > 0) {
            foreach ($this->el as $name => $element_list) {
                foreach ($element_list as $element) {
                    $thiselement->appendChild($element->ToXML($thiselement));
                }
            }
        }
        $ret = null;

        /** TODO: XRR export
         * if(in_array($this->RawTagName(), XrrRootDocTypes::toArray()) || $this instanceof XRRBuilder)
         * {
         * $ret = self::$gendoc->saveXML(self::$gendoc);
         * }
         *
         * else
         * {
         * $ret = $thiselement;
         * }
         */
        $ret = $thiselement;
        return $ret;
    }



    public function RawTagName()
    {
        return $this->rawTagName;
    }

    private function TransformClassName($tagname)
    {
        $ret = $tagname;
        switch ($tagname) {
            case "PrimaryContactInfo":
            case "AlternateContactInfo":
                if ($this->rawTagName === "PersonDetails")
                    $ret = "ContactInfo";
                break;
            case "Image":
                if ($this->rawTagName === "PersonDetails") {
                    $ret = "PersonImage";
                }
                break;
        }
        return $ret;
    }

}
