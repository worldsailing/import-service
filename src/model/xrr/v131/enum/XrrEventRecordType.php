<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v131\enum;

use Model\AbstractEnum;

/**
 * Class XrrEventRecordType
 * @package Model\xrr\v131\enum
 */
class XrrEventRecordType extends AbstractEnum
{
    /**
     *
     */
    const REGATTA = "IndividualRegatta";
    /**
     *
     */
    const TRNMNT = "RegattaSeries";

    /**
     * @var array
     */
    protected static $valueMap = [
        "REGATTA" => self::REGATTA,
        "INDIVIDUAL_REGATTA" => self::REGATTA,
        "INDIVIDUALREGATTA" => self::REGATTA,

        "REAGATTA_SERIES" => self::REGATTA_SERIES,
        "REGATTASERIES" => self::REGATTA_SERIES,
        "SERIES" => self::REGATTA_SERIES,
        "TRNMNT" => self::REGATTA_SERIES
    ];
}
