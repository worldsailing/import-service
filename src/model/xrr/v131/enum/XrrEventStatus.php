<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v131\enum;

use Model\AbstractEnum;

/**
 * Class XrrEventStatus
 * @package Model\xrr\v131\enum
 */
class XrrEventStatus extends AbstractEnum
{
    const PLANNED = 'planned';
    const SCHEDULED = 'scheduled';
    const INPROGRESS = 'inprogress';
    const FINISHED = 'finished';
    const RESULT = 'result';

    /**
     * @var array
     */
    protected static $valueMap = [
        "PLANNED" => self::PLANNED,
        "SCHEDULED" => self::SCHEDULED,
        "INPROGRESS" => self::INPROGRESS,
        "FINISHED" => self::FINISHED,
        "RESULT" => self::RESULT
    ];
}
