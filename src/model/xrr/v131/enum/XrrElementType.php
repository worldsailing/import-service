<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v131\enum;

use Model\AbstractEnum;

/**
 * Class XrrElementType
 * @package Model\xrr\v131\enum
 */
class XrrElementType extends AbstractEnum
{
    const PERSON = "Person";
    const BOAT = "Boat";
    const TEAM = "Team";
    const EVENT = "Event";
    const CREW = "Crew";
    const REGATTA_SERIES_RESULT = "RegattaSeriesResult";
    const DIVISION = "Division";
    const RACE = "Race";
    const RACE_RESULT = "RaceResult";
    const SERIES_RESULT = "SeriesResult";
    const TR_RESULT = "TRResult";

    /**
     * @var array
     */
    protected static $valueMap = [

    ];
}
