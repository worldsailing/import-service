<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v131\enum;

use Model\AbstractEnum;

class XrrProcessStatus extends AbstractEnum
{
    const PROCESSING = "PROCESSING";
    const INACTIVE = "INACTIVE";

    protected static $valueMap = [
        "PROCESSING" => self::PROCESSING,
        "IN_PROGRESS" => self::PROCESSING,
        "INPROGRESS" => self::PROCESSING,
        "IN PROGRESS" => self::PROCESSING,

        "INACTIVE" => self::INACTIVE
    ];
}
