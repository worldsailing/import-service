<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v131\enum;

use Model\AbstractEnum;

/**
 * Class XrrBoatStatus
 * @package Model\xrr\v131\enum
 */
class XrrBoatStatus extends AbstractEnum
{
    const ARB = "ARB";
    const BFD = "BFD";
    const DGM = "DGM";
    const DNC = "DNC";
    const DNE = "DNE";
    const DNF = "DNF";
    const DNS = "DNS";
    const DPI = "DPI";
    const DSQ = "DSQ";
    const OCS = "OCS";
    const PTS = "PTS";
    const RAF = "RAF";
    const RDG = "RDG";
    const RET = "RET";
    const SCP = "SCP";
    const STP = "STP";
    const UFD = "UFD";
    const ZFP = "ZFP";

    /**
     * @var array
     */
    protected static $valueMap = [

    ];
}
