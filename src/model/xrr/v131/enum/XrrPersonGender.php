<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v131\enum;

use Model\AbstractEnum;

/**
 * Class XrrPersonGender
 * @package Model\xrr\v131\enum
 */
class XrrPersonGender extends AbstractEnum
{

    /**
     *
     */
    const MALE = "M";
    /**
     *
     */
    const FEMALE = "F";

    /**
     * @var array
     */
    protected static $valueMap = array(
        "MALE" => self::MALE,
        "MAN" => self::MALE,
        "MEN" => self::MALE,
        "M" => self::MALE,

        "FEMALE" => self::FEMALE,
        "WOMAN" => self::FEMALE,
        "WOMEN" => self::FEMALE,
        "W" => self::FEMALE,
        "F" => self::FEMALE,
    );

}
