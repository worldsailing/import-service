<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v131\enum;

use Model\AbstractEnum;

/**
 * Class XrrDivisionType
 * @package Model\xrr\v131\enum
 */
class XrrDivisionType extends AbstractEnum
{

    /**
     *
     */
    const MATCH_RACE = "1";
    /**
     *
     */
    const FLEET_RACE = "2";
    /**
     *
     */
    const TEAM_RACE = "3";
    /**
     *
     */
    const OFFSHORE = "4";
    /**
     *
     */
    const OCEANIC = "5";

    /**
     * @var array
     */
    protected static $valueMap = array(
        "FLEET_RACE" => self::FLEET_RACE,
        "FLEETRACE" => self::FLEET_RACE,
        "FLEET RACE" => self::FLEET_RACE,
        "F" => self::FLEET_RACE,

        "TEAM_RACE" => self::TEAM_RACE,
        "T" => self::TEAM_RACE,
        "TEAMTRACE" => self::TEAM_RACE,
        "TEAM RACE" => self::TEAM_RACE,

        "MATCH_RACE" => self::MATCH_RACE,
        "M" => self::MATCH_RACE,
        "MATCHRACE" => self::MATCH_RACE,
        "MATCH RACE" => self::MATCH_RACE,

        "OFFSHORE" => self::OFFSHORE,

        "OCEANIC" => self::OCEANIC
    );

}
