<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v131\enum;

use Model\AbstractEnum;

/**
 * Class XrrBoatGender
 * @package Model\xrr\v131\enum
 */
class XrrBoatGender extends AbstractEnum
{

    /**
     *
     */
    const MALE = "Male";
    /**
     *
     */
    const FEMALE = "Female";
    /**
     *
     */
    const MIXED = "Mixed";


    /**
     * @var array
     */
    protected static $valueMap = array(
        "MALE" => self::MALE,
        "MAN" => self::MALE,
        "MEN" => self::MALE,
        "M" => self::MALE,
        "2" => self::MALE,

        "FEMALE" => self::FEMALE,
        "WOMAN" => self::FEMALE,
        "WOMEN" => self::FEMALE,
        "F" => self::FEMALE,
        "W" => self::FEMALE,
        "3" => self::FEMALE,

        "MIXED" => self::MIXED,
        "X" => self::MIXED,
        "1" => self::MIXED,
        "6" => self::MIXED
    );

}
