<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v131\enum;

use Model\AbstractEnum;

/**
 * Class XrrCrewPosition
 * @package Model\xrr\v131\enum
 */
class XrrCrewPosition extends AbstractEnum
{
    /**
     *
     */
    const SKIPPER = "S";
    /**
     *
     */
    const CREW = "C";

    /**
     * @var array
     */
    protected static $valueMap = [
        "SKIPPER" => self::SKIPPER,
        "SKIP" => self::SKIPPER,
        "SK"  => self::SKIPPER,
        "CPT." => self::SKIPPER,
        "CPT" => self::SKIPPER,
        "CAPT" => self::SKIPPER,
        "CAPTAIN" => self::SKIPPER,
        "CAPT." => self::SKIPPER,
        "CPN" => self::SKIPPER,
        "CPN." => self::SKIPPER,

        "CREW" => self::CREW
    ];

}
