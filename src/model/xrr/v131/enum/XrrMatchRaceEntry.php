<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v131\enum;

use Model\AbstractEnum;

/**
 * Class XrrMatchRaceEntry
 * @package Model\xrr\v131\enum
 */
class XrrMatchRaceEntry extends AbstractEnum
{
    /**
     *
     */
    const PORT = "Port";
    /**
     *
     */
    const STARBOARD = "Starboard";

    /**
     * @var array
     */
    protected static $valueMap = [
        "PORT" => self::PORT,
        "P" => self::PORT,
        "L" => self::PORT,

        "STARBOARD" => self::STARBOARD,
        "S" => self::STARBOARD,
        "R" => self::STARBOARD
    ];
}
