<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v131\element;

use Model\xrr\v131\AbstractXrrModel;
use Model\xrr\v131\enum\XrrPersonGender;

class XrrPerson extends AbstractXrrModel
{
    protected function ListInternElements()
    {
        return array("PersonDetails", "RaceOfficialCertification", "Other");
    }

    public function SourceId($value = null)
    {
        return strtoupper($this->Attribute("PersonID", $value));
    }

    public function PersonId($value = null)
    {
        return strtoupper($this->SourceId($value));
    }

    public function ISAFID($value = null)
    {
        return strtoupper($this->Attribute("IFPersonID", $value));
    }

    public function FamilyName($value = null)
    {
        return $this->Attribute("FamilyName", $value);
    }

    public function GivenName($value = null)
    {
        return $this->Attribute("GivenName", $value);
    }

    public function IOCNationCode($value = null)
    {
        return $this->Attribute("NOC", $value);
    }

    public function Gender($value = null)
    {
        if($value !== null)
        {
            $value = XrrPersonGender::toXrr($value);
        }
        return XrrPersonGender::ToXrr($this->Attribute("Gender", $value));
    }

    public function ClassificationGroup($value = null)
    {
/** TODO: XRR export, Classification connection
        if($value !== null)
        {
            if(!class_exists("DataAccessCfcnConfig"))
            {
                $dpars = new DirectoryParser(SHAREDPATH . "/isaf/public_html/sailorsclassification/system", null, null, array("php"));
                $dpars->RequireAllFiles();
            }
            $cfcngroups = DataAccessCfcnConfig::GetClassificationGroups(true);
            if(!in_array($value, $cfcngroups))
            {
                $value = null;
            }
        }
 */
        return $this->Attribute("ClassificationGroup", $value);
    }

    public function ClassificationStatus($value = null)
    {
        return $this->Attribute("ClassificationStatus", $value);
    }

    public function ClassificationExpiry($value = null)
    {
/** TODO: XRR export, Classification connection
        if($value !== null)
        {
            $dal = new DataAccessGlobal("isaf");
            $value = $dal->UnixToDate($value);
        }
 */
        return $this->Attribute("ClassificationExpiry", $value);
    }

    public function ContactPhone($value = null)
    {
        return $this->Attribute("ContactPhone", $value);
    }

    public function ContactEmail($value = null)
    {
        return $this->Attribute("ContactEmail", $value);
    }

    public function PersonDetails()
    {
        $ret = $this->Elements("PersonDetails");
        if(count($ret) > 0)
        {
            $ret = $ret[0];
        }
        else
        {
            $ret = null;
        }
        return $ret;
    }

    public function RaceOfficialCertification()
    {
        return $this->Elements("RaceOfficialCertification");
    }

    public function Other()
    {
        return $this->Elements("Other");
    }

}
