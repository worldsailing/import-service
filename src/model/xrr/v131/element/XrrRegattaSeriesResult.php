<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v131\element;

use Model\xrr\v131\AbstractXrrModel;

/**
 * Class XrrRegattaSeriesResult
 * @package Model\xrr\v131\element
 */
class XrrRegattaSeriesResult extends AbstractXrrModel
{

    protected function ListInternElements()
    {
        return array("EventID", "Other");
    }

    public function EventIds()
    {
        $list = $this->Elements("EventID");
        foreach($list as $lidx => $element)
        {
            $list[$lidx] = $element->nodeValue;
        }
        return $list;
    }

    public function Other()
    {
        return $this->Elements("Other");
    }

    public function TeamId($value = null)
    {
        return $this->Attribute("TeamID", $value);
    }

    public function TotalPoints($value = null)
    {
        return $this->Attribute("TotalPoints", $value);
    }

    public function Rank($value = null)
    {
        return $this->Attribute("Rank", $value);
    }
}
