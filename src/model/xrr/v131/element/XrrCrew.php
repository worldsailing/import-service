<?php

/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace Model\xrr\v131\element;

use Model\xrr\v131\enum\XrrCrewPosition;

class XrrCrew extends XrrParticipant
{
    public function Position($value = null)
    {
        if ($value !== null) {
            $value = XrrCrewPosition::toXrr($value);
        }
        return XrrCrewPosition::toXrr($this->Attribute("Position", $value));
    }

}
