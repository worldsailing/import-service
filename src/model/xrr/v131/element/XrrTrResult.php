<?php

/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace Model\xrr\v131\element;

use Model\xrr\v131\AbstractXrrModel;

/**
 * Class XrrTrResult
 * @package Model\xrr\v131\element
 */
class XrrTrResult extends AbstractXrrModel
{
    protected function ListInternElements()
    {
    }

    public function RaceId($value = null)
    {
        return strtoupper($this->Attribute("RaceID", $value));
    }

    public function TeamId($value = null)
    {
        return strtoupper($this->Attribute("TeamID", $value));
    }

    public function NetPoints($value = null)
    {
        return $this->Attribute("NetPoints", $value);
    }

    public function TeamPoints($value = null)
    {
        return $this->Attribute("TeamPoints", $value);
    }

    public function Rank($value = null)
    {
        return $this->Attribute("Rank", $value);
    }
}
