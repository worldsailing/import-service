<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Model\xrr\v131\element;

use Model\xrr\v131\AbstractXrrModel;
use Helper\XrrHelper;

/**
 * Class XrrSeriesResult
 * @package Model\xrr\v131\element
 */
class XrrSeriesResult extends AbstractXrrModel
{
    protected function ListInternElements()
    {
        return array("Other");
    }

    public function Other()
    {
        return $this->Elements("Other");
    }

    public function TeamId($value = null)
    {
        return strtoupper($this->Attribute("TeamID", $value));
    }

    public function NetPoints($value = null)
    {
        return $this->Attribute("NetPoints", $value);
    }

    public function TotalPoints($value = null)
    {
        return $this->Attribute("TotalPoints", $value);
    }

    public function Rank($value = null)
    {
        return $this->Attribute("Rank", $value);
    }

    public function Tied($value = null)
    {
        if($value !== null)
        {
            $value = XrrHelper::boolIn($value);
        }
        return XrrHelper::boolOut($this->Attribute("Tied", $value));
    }
}
