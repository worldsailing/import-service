<?php

/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace Model\xrr\v131\element;

use Model\xrr\v131\AbstractXrrModel;
use \DOMDocument;

class SailingXrr extends AbstractXrrModel
{
    protected $queryType;
    private $extSchemaLoc;
    private $validationErrors;

    public function Persons()
    {
        return $this->Elements("Person");
    }

    public function Boats()
    {
        return $this->Elements("Boat");
    }

    public function Teams()
    {
        return $this->Elements("Team");
    }

    public function Events()
    {
        return $this->Elements("Regatta");
    }

    public function Other()
    {
        return $this->Elements("Other");
    }

    public function ExtSchemaLocation()
    {
        return $this->extSchemaLoc;
    }

    public function Validate($schema)
    {
        $ret = false;
        if ($this->xmlDoc) {
            libxml_use_internal_errors(true);
            $ret = $this->xmlDoc->schemaValidateSource($schema);
            if (!$ret) {
                $this->validationErrors = libxml_get_errors();
                foreach ($this->validationErrors as $eidx => $error) {
                    $this->validationErrors[$eidx] = $error->message . " line: " . $error->line;
                }
            }
            libxml_use_internal_errors(false);
        }
        return $ret;
    }

    public function ValidationErrors()
    {
        return $this->validationErrors;
    }

    public function XRRVersion($value = null)
    {
        return $this->Attribute("Version", $value);
    }

    public function DateTime($value = null)
    {
        $ret = $this->Attribute("DateTime", $value);
        if (!$ret) {
            $date = $this->Date();
            $time = $this->Time();
            if ($date && $time) {
                $ret = implode("T", array($date, $time));
            }
        }
        return $ret;
    }

    public function Date($value = null)
    {
        return $this->LegacyDateParse($this->Attribute("Date", $value));
    }

    public function Time($value = null)
    {
        return $this->LegacyTimeParse($this->Attribute("Time", $value));
    }

    public function Type($value = null)
    {
        return $this->Attribute("Type", $value);
    }

    public function ToXML()
    {
        self::$gendoc = null;
        $gendoc = new DOMDocument("1.0");
        $gendoc->formatOutput = true;
        $doccontent = parent::ToXML();
        $gendoc->loadXML($doccontent);
        return $gendoc->saveXML();
    }

    public function PersistDescription()
    {
        $ret = "";
        /*
        $dal = new DataAccessXRRAdmin();

        foreach($this->el as $el_name => $el_value)
        {
            $ret .= string_format("[{0}: {1}]\n", array($el_name, count($el_value)));
        }
        $events = $this->Elements("Regatta");

        if(count($events) > 0)
        {
            foreach($events as $event)
            {
                // @var $event XRR_Regatta
                $ifevtid = $event->IsafRegattaId();
                if($ifevtid)
                {
                    if(strpos($ifevtid, "-") !== false)
                    {
                        $ifevtid = explode("-", $ifevtid);
                        $ifevtid = $ifevtid[1];
                    }

                    switch($event->EventType())
                    {
                        case XRREventRecordType::TRNMNT:
                            $ret .= "Tournament ID: " . $ifevtid;
                            $tourn = $dal->GetAllFieldsByKeyListSingle("Tournaments", array("TournId" => $ifevtid));
                            if($tourn)
                            {
                                $ret .= " (" . $tourn->TournName . ")";
                            }
                            $ret .= "\n";
                            break;

                        case XRREventRecordType::REGATTA:
                            $ret .= "\nRegatta ID: " . $ifevtid;
                            $rgta = $dal->GetAllFieldsByKeyListSingle("Regattas", array("RgtaIsafId" => $ifevtid, "RgtaApvlStatus" => 'Y'));
                            if(!$rgta)
                            {
                                $rgta = $dal->GetAllFieldsByKeyListSingle("Regattas", array("RgtaId" => $ifevtid, "RgtaApvlStatus" => 'Y'));
                            }
                            if($rgta)
                            {
                                $ret .= " - " . $rgta->RgtaName;
                            }
                            $ret .= "\n";
                            $divisions = $event->Divisions();
                            foreach($divisions as $division)
                            {
                                $ret .= string_format("[{0}-{1}{5}: {2} RaceResult; {3} SeriesResult; {4} TRResult]\n", array($division->IFClassCode(), $division->Gender(), count($division->RaceResults()), count($division->SeriesResults()), count($division->TeamRaceResults()),
                                    ($division->IsDisabled() ? "-d" : "")));
                            }
                            break;
                    }
                }
            }
        }
        else
        {
            $ret .= "(none)\n";
        }
         */
        return $ret;

    }

    protected function LoadXML()
    {

        $this->data = utf8_encode($this->data);
        libxml_use_internal_errors(true);

        $this->validationErrors = libxml_get_errors();
        foreach ($this->validationErrors as $eidx => $error) {
            $this->validationErrors[$eidx] = $error->message . " line: " . $error->line;
        }
        libxml_use_internal_errors(false);

    }

    protected function Populate()
    {
        $searchFor = array("SchemaLocation", "schemaLocation", "schemalocation", "SchemaLocation");
        $schpos = null;
        $sstring = null;
        foreach ($searchFor as $sstring) {
            $schpos = strpos($this->data, $sstring);
            if ($schpos !== false) {
                break;
            }
        }

        $schstr = null;
        if ($schpos !== false) {
            $schstr = substr($this->data, ($schpos + strlen($sstring)));
            $schstr = substr($schstr, 0, (strpos($schstr, ".xsd") + strlen(".xsd")));
            $schstr = trim($schstr);
            $schstr = trim($schstr, "'\"= ");
        }
        $this->extSchemaLoc = $schstr;
        parent::Populate();
    }

    protected function ListInternElements()
    {
        return array("Person", "Boat", "Team", "Regatta", "Other");
    }
}

?>

