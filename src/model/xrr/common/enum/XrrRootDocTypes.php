<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace Model\xrr\common\enum;

use Model\AbstractEnum;

class XrrRootDocTypes extends AbstractEnum
{
    const SAILING_XRR = "SailingXRR";
    const REGATTA_RESULT = "RegattaResult";

    /**
     * @var array
     */
    protected static $valueMap = [
        "SAILING_XRR" => self::SAILING_XRR,
        "SAILINGXRR" => self::SAILING_XRR,
        "SAILING XRR" => self::SAILING_XRR,

        "REGATTA_RESULT" => self::REGATTA_RESULT,
        "REGATTARESULT" => self::REGATTA_RESULT,
        "REGATTA RESULT" => self::REGATTA_RESULT
    ];
}
