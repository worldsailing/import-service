<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace Model\xrr\common\enum;

use Model\AbstractEnum;

class XrrVersions extends AbstractEnum
{
    const V130 = "1.3.0";
    const V131 = "1.3.1";
    const V201 = "2.0.1";

    /**
     * @var array
     */
    protected static $valueMap = [
        "1.3.0" => self::V130,
        "V130" => self::V130,
        "V1.3.0" => self::V130,
        "V 1.3.0" => self::V130,
        "1.3" => self::V130,
        "13" => self::V130,
        "V13" => self::V130,
        "V1.3" => self::V130,
        "V 1.3" => self::V130,

        "1.3.1" => self::V131,
        "V131" => self::V131,
        "V1.3.1" => self::V131,
        "V 1.3.1" => self::V131,

        "2.0.1" => self::V201,
        "V201" => self::V201,
        "V2.0.1" => self::V201,
        "V 2.0.1" => self::V201,

    ];
}
