<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace Model\xrr\common\enum;

use Model\AbstractEnum;

class XrrComparisonPatterns extends AbstractEnum
{
    const UTF8 = "UTF-8";
    const ISO88591 = "ISO-8859-1";

    /**
     * @var array
     */
    protected static $valueMap = [

    ];
}


