<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace Model\xrr;

interface XrrImportModelInterface
{
    /**
     * @return string
     */
    public function getData();

    /**
     * @param $data
     * @return XrrImportModelInterface
     */
    public function loadData($data);

    /**
     * @return array
     */
    public function getRegattas();

    /**
     * @param array $regattas
     * @return XrrImportModelInterface
     */
    public function setRegattas($regattas);

    /**
     * @return array
     */
    public function getTournaments();

    /**
     * @param array $tournaments
     * @return XrrImportModelInterface
     */
    public function setTournaments($tournaments);

    /**
     * @return array
     */
    public function getTeams();

    /**
     * @param mixed $teams
     * @return XrrImportModelInterface
     */
    public function setTeams($teams);

    /**
     * @param mixed $key
     * @param $team
     * @return XrrImportModelInterface
     */
    public function addTeam($key, $team);

    /**
     * @return bool
     */
    public function isSimulate();

    /**
     * @param bool $simulate
     * @return XrrImportModelInterface
     */
    public function setSimulate($simulate);

    /**
     * @return \worldsailing\api\response\ErrorEntity[]
     */
    public function getValidationMessages();

    /**
     * @param \worldsailing\api\response\ErrorEntity[]|\worldsailing\api\response\ErrorList $validationMessages
     * @return XrrImportModelInterface
     */
    public function setValidationMessages($validationMessages);

    /**
     * @param array|string $validationMessage
     * @return XrrImportModelInterface
     */
    public function addValidationMessages($validationMessage);

    /**
     * @return bool
     */
    public function isValid();

    /**
     * @return bool
     */
    public function isCheckSailorNames();

    /**
     * @param bool $checkSailorNames
     */
    public function setCheckSailorNames($checkSailorNames);

    /**
     * @return string
     */
    public function getNameComparisonCharCode();

    /**
     * @param string $nameComparisonCharCode
     */
    public function setNameComparisonCharCode($nameComparisonCharCode);

    /**
     * @return array
     */
    public function getSailorSourceIds();

    /**
     * @param array $sailorSourceIds
     * @return XrrImportModel
     */
    public function setSailorSourceIds($sailorSourceIds);

    /**
     * @param $id
     * @return $this
     */
    public function addSailorSourceId($id);

    /**
     * @return array
     */
    public function getBoatSourceIds();

    /**
     * @param array $boatSourceIds
     * @return XrrImportModel
     */
    public function setBoatSourceIds($boatSourceIds);

    /**
     * @param $id
     * @return $this
     */
    public function addBoatSourceId($id);

    /**
     * @return array
     */
    public function getTeamSourceIds();

    /**
     * @param array $teamSourceIds
     * @return XrrImportModel
     */
    public function setTeamSourceIds($teamSourceIds);

    /**
     * @param $id
     * @return $this
     */
    public function addTeamSourceId($id);

    /**
     * @param $version
     * @return mixed
     */
    public function getSchema($version);

    /**
     * @param array $isafIds
     * @param bool $reArrange
     * @return array|bool
     */
    public function getMembers($isafIds = [], $reArrange = false);

}
