<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Provider;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Service\xrr\XrrImportInterface;
use Service\xrr\v201\XrrImportService as XrrImportServiceV201;
use worldsailing\Helper\WsHelper;
use Model\xrr\common\enum\XrrVersions;
/**
 * Class XrrServiceProvider
 * @package Provider
 */
class XrrServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        /**
         * @param Container $app
         * @return XrrImportInterface
         * @throws \Exception
         */
        $app['service.xrr.import'] = function ($app) {
            try {
                switch ($app['xrr.config']['input']['xrr_root_tag_attr']['Version']) {
                    case XrrVersions::V201:
                        return new XrrImportServiceV201($app);
                        break;
                    default :
                        throw new \Exception('Unsupported XRR version [' . $app['xrr.config']['input']['xrr_root_tag_attr']['Version'] . ']');
                        break;
                }
            } catch (\Exception $e) {
                $app['monolog']->error('Configuration error', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
                throw new \Exception('Configuration error');
            }
        };
    }
}

