<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace Service\xrr\v201;

use Helper\XrrHelper;
use Model\xrr\v201\element\XrrCorner;
use Model\xrr\v201\element\XrrCourse;
use Model\xrr\v201\element\XrrMarkSequence;
use Model\xrr\v201\element\XrrRace;
use Model\xrr\v201\element\XrrRaceResult;
use Model\xrr\v201\element\XrrRegatta;
use Model\xrr\v201\element\XrrSailingXrr;
use Model\xrr\v201\enum\XrrBoatStatus;
use Model\xrr\v201\enum\XrrEventClass;
use Model\xrr\v201\enum\XrrEventGender;
use Model\xrr\v201\enum\XrrEventRecordType;
use Model\xrr\v201\enum\XrrEventType;
use Model\xrr\v201\enum\XrrPersonGender;
use Model\xrr\v201\enum\XrrRaceStatus;
use Model\xrr\v201\enum\XrrDivisionType;
use Model\xrr\v201\XrrImportModel;
use Model\xrr\v201\XrrRoot;
use Silex\Application;
use Model\xrr\common\enum\XrrVersions;
use Service\xrr\XrrImportInterface;
use Model\xrr\XrrImportModelInterface;
use worldsailing\Api\response\xrr\XrrImportResultSet;
use worldsailing\Common\Exception\WsServiceException;
use worldsailing\Helper\FileHelper;

class XrrImportService implements XrrImportInterface
{

    /**
     * @var \Silex\Application
     */
    private $app;

    /**
     * XrrImportService constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return  XrrVersions::V201;
    }

    /**
     * @param $app
     * @param bool $simulation
     * @return XrrImportModel
     */
    public function xrrImportModel($app, $simulation = false)
    {
        return (new XrrImportModel($app))->setSimulate($simulation);
    }

    /**
     * @param XrrImportModelInterface $model
     * @param bool $doPersistence
     * @param bool $addToApprovalQueue
     * @return array
     * @throws WsServiceException
     */
    public function processImport(XrrImportModelInterface &$model, $doPersistence = false, $addToApprovalQueue = false)
    {
        $report = [
            'validation' => [
                'structure' => true,
                'schema' => true,
                'persons' => true,
                'boats' => true,
                'teams' => true,
                'events' => true
            ],
            'persistence' => [],
            'error' => []
        ];
        $sailingXrr = new XrrRoot( $model->getData() );
        $sailingXrr = $sailingXrr->Document();

        /**
         * Basic structure validation
         */
        if(!strtotime($sailingXrr->DateTime())) {
            $model->addValidationMessages("The timestamp on this document is missing or invalid", 'structure');
            $report['validation']['structure'] = false;
        }
        foreach($sailingXrr->Events() as $event)
        {
            /** @var \Model\xrr\v201\element\XrrRegatta $event */
            foreach($event->Divisions() as $division)
            {
                /** @var \Model\xrr\v201\element\XrrDivision $division */
                if ($division->DivisionType()) {
                    if ($division->DivisionType() !== XrrDivisionType::TEAM_RACE && count($division->TeamRaceResults()) > 0) {
                        $model->addValidationMessages("Division@Type '" . $division->DivisionType() . "' has TRResult entities assigned.  Division@DivisionID: " . $division->DivisionId() . "; IFClassID: " . $division->IFClassCode() . "; Gender: " . $division->Gender(), 'structure');
                        $report['validation']['structure'] = false;
                    }
                }
            }
        }

        /**
         * Get schema by version number
         */
        /** @var \worldsailing\Isaf\model\XrrSchemaDoc $schema */
        $schema = $model->getSchema($this->getVersion());
        if ($schema) {
            if (ENVIRONMENT === 'dev') {
                FileHelper::saveToFile(PATH_LOG . '/../export/schema.' . $this->getVersion() . '.xsd', XrrHelper::unSerialise($schema->getXRRSchDocContent()));
            }
            /**
             * Schema validation
             */
            if ($sailingXrr->Validate(XrrHelper::unSerialise($schema->getXRRSchDocContent())) === true) {
                /**
                 * Sailor validation
                 */
                $report['validation']['persons'] = $this->validatePersons($model, $sailingXrr);

                /**
                 * Boat validation
                 */
                $report['validation']['boats'] = $this->validateBoats($model, $sailingXrr);

                /**
                 * Team validation
                 */
                $report['validation']['teams'] = $this->validateTeams($model, $sailingXrr);

                /**
                 * Event validation
                 */
                $report['validation']['events'] = $this->validateEvents($model, $sailingXrr);

            } else {
                /**
                 * Invalid schema
                 */
                foreach ($sailingXrr->ValidationErrors() as $key => $validationError) {
                    $model->addValidationMessages($validationError, 'schema');
                }
                $report['validation']['schema'] = false;
            }
        } else {
            /**
             * Missing basic elements, not valid xrr
             */
            $model->addValidationMessages('Missing schema for version [' . $this->getVersion() . ']', 'schema');
            $report['validation']['structure'] = false;
        }

        if ($model->isValid()) {
            /**
             * Persist XRR
             */



        } else {
            $this->app['monolog']->debug('Invalid XRR\\n');
            foreach ($model->getValidationMessages() as $error) {
                $this->app['monolog']->debug('Error: ' . $error->toJson());
            }
            $report['error'] = $model->getValidationMessages();

            $e = new WsServiceException('Validation error', 400);
            $e->__setData(new XrrImportResultSet('result', $report));

            throw $e;
        }

        return $report;
    }

    /**
     * @param XrrImportModelInterface $model
     * @param XrrSailingXrr $sailingXrr
     * @return bool
     */
    protected function validatePersons(XrrImportModelInterface &$model, XrrSailingXrr &$sailingXrr)
    {
        $result = true;
        $sailors = $sailingXrr->Persons();
        $isafIds = [];
        /** @var \Model\xrr\v201\element\XrrPerson $sailor */
        foreach($sailors as $sailor)
        {
            $model->addSailorSourceId($sailor->SourceId());
            $isafId = trim($sailor->ISAFID());
            if ($isafId == '') {
                $model->addValidationMessages("No Person@IFPersonID has been provided.  Person@NOC: " . $sailor->IOCNationCode() . "; Person@PersonID: " . $sailor->PersonId() . "; Person@GivenName: " . $sailor->GivenName() . "; Person@FamilyName: " . $sailor->FamilyName(), 'persons');
                $result = false;
            } else {
                if(in_array($isafId, $isafIds)){
                    $model->addValidationMessages("Person@IFPersonID '" . $sailor->ISAFID() . "' has been associated with multiple Person elements", 'persons');
                    $result = false;
                }
            }
            $isafIds[] = $isafId;
        }

        $members = $model->getBiogs($isafIds, true);
        if ($members && (count($members) < count($isafIds))) {

            foreach($sailors as $sailor) {
                $isafId = trim($sailor->ISAFID());
                if ($isafId != '') {
                    if (! array_key_exists($isafId, $members)) {
                        $model->addValidationMessages("Invalid sailor ID '" . $isafId . "'.  Person@NOC: " . $sailor->IOCNationCode() . "; Person@PersonID: " . $sailor->PersonId() . "; Person@GivenName: " . $sailor->GivenName() . "; Person@FamilyName: " . $sailor->FamilyName(), 'persons');
                        $result = false;
                    } else {
                        if ($model->isCheckSailorNames()) {
                            if((strtoupper(htmlentities(trim($members[$isafId]->getBiogSurname()), ENT_COMPAT, $model->getNameComparisonCharCode())) !== strtoupper(htmlentities(trim($sailor->FamilyName()), ENT_COMPAT, $model->getNameComparisonCharCode()))) || (strtoupper(htmlentities(trim($members[$isafId]->getBiogFirstName()), ENT_COMPAT, $model->getNameComparisonCharCode())) !== strtoupper(htmlentities(trim($sailor->GivenName()), ENT_COMPAT, $model->getNameComparisonCharCode())))) {

                                $model->addValidationMessages("Sailor name mismatch - Person@PersonID: " . $sailor->PersonId() . "; Person@GivenName: " . $sailor->GivenName() . ", Member-FirstName: " . $members[$isafId]->getBiogFirstName() . "; Person@FamilyName: " . $sailor->FamilyName() . ", Member-Surname: " . $members[$isafId]->getBiogSurname() . "; Person@NOC: " . $sailor->IOCNationCode(), 'persons');
                                $result = false;
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param XrrImportModelInterface $model
     * @param XrrSailingXrr $sailingXrr
     * @return bool
     */
    protected function validateBoats(XrrImportModelInterface &$model, XrrSailingXrr &$sailingXrr)
    {
        $result = true;
        $boats = $sailingXrr->Boats();
        $boatIds = array();
        foreach($boats as $boat) {
            if(in_array($boat->BoatId(), $boatIds)) {
                $model->addValidationMessages("Boat@BoatID '" . $boat->BoatId() . "' has been associated with multiple Boat elements", 'boats');
                $result = false;
            }
            $boatIds[] = $boat->BoatId();
        }
        $model->setBoatSourceIds($boatIds);
        return $result;
    }

    /**
     * @param XrrImportModelInterface $model
     * @param XrrSailingXrr $sailingXrr
     * @return bool
     */
    protected function validateTeams(XrrImportModelInterface &$model, XrrSailingXrr &$sailingXrr)
    {
        $result = true;
        $teams = $sailingXrr->Teams();
        $teamIds = array();
        foreach($teams as $team) {
            if(in_array($team->TeamId(), $teamIds)) {
                $model->addValidationMessages("Team@TeamID '" . $team->TeamId() . "' has been associated with multiple Team elements", 'teams');
                $result = false;
            }
            $teamIds[] = $team->TeamId();
            $model->addTeam($team->TeamId(), $team);

            if(strlen($team->BoatId()) > 0 && !in_array($team->BoatId(), $model->getBoatSourceIds())) {
                $model->addValidationMessages("Team@BoatID " . $team->BoatId() . " is not present in the Boat entities. Team@TeamID: " . $team->TeamId(), 'teams');
                $result = false;
            }
            /**
             * TODO: test
             * Nation code validation has been skipped, because it validated at XML schema validation already.
             * At this moment we don't have an attribute to mark that is that nation a member of World Sailing.
             * E.g.: See "Benin" in country table. It's still valid, even you can choose that on back-end.
             * In this case, one validation in xml schema is enough.
             *
             * if($team->IOCNationCode() && !IsafCountries::FromIOCNatCode($team->IOCNationCode())) {
             *  $model->addValidationMessages("Team@NOC '" . $team->IOCNationCode() . "' is not a valid nation code");
             *  $result = false;
             * }
             */

            $crew = $team->Crew();

            foreach($crew as $cmemb)
            {

                if(!in_array($cmemb->PersonId(), $model->getSailorSourceIds())) {
                    $model->addValidationMessages("Crew@PersonID " . $cmemb->PersonId() . " is not present in the Person entities.  Team@TeamID: " . $team->TeamId(), 'teams');
                    $result = false;
                }
            }
        }
        $model->setTeamSourceIds($teamIds);
        return $result;
    }

    /**
     * @param XrrImportModelInterface $model
     * @param XrrSailingXrr $sailingXrr
     * @return bool
     */
    protected function validateEvents(XrrImportModelInterface &$model, XrrSailingXrr &$sailingXrr)
    {
        $result = true;

        $events = $sailingXrr->Events();
        /** @var XrrRegatta $event */
        foreach($events as $event) {
            $eventId = $event->IsafRegattaId();
            if (strpos($eventId, "-") !== false) {
                $recordId = explode("-", $eventId);
                $recordId = count($recordId > 1) ? $recordId[1] : $recordId[0];
            } else {
                $recordId = $eventId;
            }

            switch($event->EventType()) {
                case XrrEventRecordType::REGATTA:
                    $record = $model->getRegattaByIsafId($eventId);

                    if(!$record) {
                        $record = $model->getRegattaById($recordId);
                    }
                    break;
                case XrrEventRecordType::TRNMNT:
                    $record = $model->getTournamentById($recordId);
                    break;
                default:
                    $record = false;
                    break;
            }

            if($record){
                switch($event->EventType())
                {
                    case XrrEventRecordType::TRNMNT:
                        if (! $this->ValidateRegattaSeries($model, $sailingXrr, $event)) {
                            $result = false;
                        }
                        break;
                    case XrrEventRecordType::REGATTA:
                        if (! $this->ValidateRegatta($model, $sailingXrr, $event)) {
                            $result = false;
                        }
                        break;
                }
            } else {
                $model->addValidationMessages("Regatta@IFRegattaID " . $event->IsafRegattaId() . " is not valid.  Regatta@RegattaID: " . $event->RegattaId(), 'events');
                $result = false;
            }
        }

        return $result;
    }

    /**
     * @param XrrImportModelInterface $model
     * @param XrrSailingXrr $sailingXrr
     * @param XrrRegatta $event
     * @return bool
     */
    protected function validateRegattaSeries(XrrImportModelInterface &$model, XrrSailingXrr &$sailingXrr, XrrRegatta $event)
    {
        $result = true;
        if(count($event->Races()) > 0 || count($event->Divisions()) > 0) {
            $model->addValidationMessages("Unable to process Race or Division entities associated with Regatta@Type '" . XrrEventType::REGATTA_SERIES . "'.  Regatta@IFRegattaID: " . $event->IsafRegattaId() . "; Regatta@RegattaID: " . $event->RegattaId(), 'events');
            $result = false;
        }

        $sResults = $event->RegattaSeriesResults();
        foreach($sResults as $sResult) {
            if($sResult->TeamId() !== null && !in_array($sResult->TeamId(), $model->getTeamSourceIds())) {
                $model->addValidationMessages("RegattaSeriesResult@TeamID " . $sResult->TeamId() . " is not present in the Team entities.  Regatta@IFRegattaID: " . $event->IsafRegattaId() . "; Regatta@RegattaID: " . $event->RegattaId(), 'events');
                $result = false;
            }

            if(count($sResult->Events()) > 0) {
                $subEvents = $sResult->Events();
                foreach($subEvents as $subEvent) {
                    if (! $this->ValidateRegatta($model, $sailingXrr, $subEvent)) {
                        $result = false;
                    }
                }
            }
        }
        return $result;
    }

    /**
     * @param XrrImportModelInterface $model
     * @param XrrSailingXrr $sailingXrr
     * @param XrrRegatta $event
     * @return bool
     */
    protected function validateRegatta(XrrImportModelInterface &$model, XrrSailingXrr &$sailingXrr, XrrRegatta $event)
    {
        $result = true;
        $eventId = $event->IsafRegattaId();
        if (strpos($eventId, "-") !== false) {
            $recordId = explode("-", $eventId);
            $recordId = count($recordId > 1) ? $recordId[1] : $recordId[0];
        } else {
            $recordId = $eventId;
        }
        if($recordId) {
            $races = $event->Races();
            $mkSeqMap = array();

            if(count($event->RegattaSeriesResults()) > 0) {
                $model->addValidationMessages("Unable to process RegattaSeriesResult entities associated with Regatta@Type '" . XrrEventType::REGATTA . "'.  Regatta@IFRegattaID: " . $event->IsafRegattaId() . "; Regatta@RegattaID: " . $event->RegattaId(), 'events');
                $result = false;
            }

            $raceIds = array();
            foreach($races as $race) {

                $markMap = [];

                /* @var $race XrrRace */
                if(!isset($markMap[$race->RaceId()])) {
                    $markMap[$race->RaceId()] = array();
                }

                if($race->_StartScheduled() && !strtotime($race->_StartScheduled())) {
                    $model->addValidationMessages("Race@StartScheduled '" . $race->_StartScheduled() . "' is not a valid DateTime format.  Race@RaceId: " . $race->RaceId() . "; Regatta@IFRegattaID: " . $event->IsafRegattaId() . "; Regatta@RegattaID: " . $event->RegattaId(), 'events');
                    $result = false;
                }

                if($race->_StartActual() && !strtotime($race->_StartActual())) {
                    $model->addValidationMessages("Race@StartActual '" . $race->_StartScheduled() . "'is not a valid DateTime format.  Race@RaceId: " . $race->RaceId() . "; Regatta@IFRegattaID: " . $event->IsafRegattaId() . "; Regatta@RegattaID: " . $event->RegattaId(), 'events');
                    $result = false;
                }

                if(!$race->StartScheduled() && !$race->StartActual()) {
                    if($race->StartDate() && $race->StartTime()) {
                        if(!strtotime(implode("T", array($race->StartDate(), $race->StartTime())))) {
                            $model->addValidationMessages("Race@StartDate and Race@StartTime do not form a valid dateTime. Race@RaceId: " . $race->RaceId() . "; Regatta@IFRegattaID: " . $event->IsafRegattaId() . "; Regatta@RegattaID: " . $event->RegattaId(), 'events');
                            $result = false;
                        }
                    }
                    else if($race->StartDate() && !$race->StartTime()) {
                        $model->addValidationMessages("Race@StartTime is missing. Race@RaceId: " . $race->RaceId() . "; Regatta@IFRegattaID: " . $event->IsafRegattaId() . "; Regatta@RegattaID: " . $event->RegattaId(), 'events');
                        $result = false;
                    }
                    else if(!$race->StartDate() && $race->StartTime()) {
                        $model->addValidationMessages("Race@StartDate is missing. Race@RaceId: " . $race->RaceId() . "; Regatta@IFRegattaID: " . $event->IsafRegattaId() . "; Regatta@RegattaID: " . $event->RegattaId(), 'events');
                        $result = false;
                    }
                }

                $raceStatus = XrrRaceStatus::fromXrr($race->RaceStatus(), false);
                if(strlen($raceStatus) > 0 && ! XrrRaceStatus::isValidValue($raceStatus)) {
                    $model->addValidationMessages("Race@RaceStatus '" . $race->RaceStatus() . "' is not valid. Race@RaceId: " . $race->RaceId() . "; Regatta@IFRegattaID: " . $event->IsafRegattaId() . "; Regatta@RegattaID: " . $event->RegattaId(), 'events');
                    $result = false;
                }

                if(in_array($race->RaceId(), $raceIds)) {
                    $model->addValidationMessages("Race@RaceID '" . $race->RaceId() . "' has been associated with multiple Race elements. Regatta@IFRegattaID: " . $event->IsafRegattaId() . "; Regatta@RegattaID: " . $event->RegattaId(), 'events');
                    $result = false;
                }
                $raceIds[] = $race->RaceId();

                /* @var $race XrrRace */
                $course = $race->Course();
                /* @var $course XrrCourse */
                if($course) {
                    $marks = $course->CompoundMarks();
                    if(count($marks) > 0) {
                        foreach($marks as $mark) {
                            /* @var $mark XRR_CompoundMark */
                            $markMap[$race->RaceId()][$mark->CompoundMarkID()] = array();
                        }
                    }

                    $mSeq = $course->MarkSequence();
                    /* @var $mSeq XrrMarkSequence */
                    if($mSeq) {
                        $corners = $mSeq->Corners();
                        if(count($corners) > 0) {
                            foreach($corners as $corner) {
                                /* @var $corner XrrCorner */
                                if(!isset($markMap[$race->RaceId()][$corner->CompoundMarkID()]) || !is_array($markMap[$race->RaceId()][$corner->CompoundMarkID()])) {
                                    $model->addValidationMessages("Corner@CompoundMarkID '" . $corner->CompoundMarkID() . "' is not present in the compound mark list.  Race@RaceId: " . $race->RaceId() . "; Regatta@IFRegattaID: " . $event->IsafRegattaId() . "; Regatta@RegattaID: " . $event->RegattaId(), 'events');
                                    $result = false;
                                } else {
                                    if(!isset($mkSeqMap[$race->RaceId()])) {
                                        $mkSeqMap[$race->RaceId()] = array();
                                    }
                                    $mkSeqMap[$race->RaceId()][] = $corner->SeqId();
                                }
                            }
                        }
                    }
                }
            }

            $raceResultIds = array();
            $divisions = $event->Divisions();

            foreach($divisions as $division)
            {
                $divisionId = strlen(trim($division->ISAFDivisionId()));

                $divisionType = XrrDivisionType::fromXrr($division->DivisionType(), false);
                $ifEvent = null;
                if($divisionId) {
                    /**
                     * Get Event by id and regatta Id.
                     */
                    $ifEvent = $model->getEventByIdAndRgtaId($division->ISAFDivisionId(), $recordId);

                    /**
                     * TODO: Xrr Export ?
                     * At this point I have no idea what former developer wanted to do.
                     *
                    if($ifEvent) {
                        if($ifEvent->getEvntTypeId() !== null) {
                            $evntType = XrrEventGender::toXRR($ifEvent->getEvntTypeId());
                            if($evntType) {
                                $division->Gender($evntType);
                            }
                        }

                        if($ifEvent->getEvntClassId() !== null) {
                            $evntClass = $model->getClassByClassId($ifEvent->getEvntClassId());
                            if($evntClass) {
                                $division->IFClassCode($evntClass->ClassCode);
                            }
                        }
                    }
                    */
                }

                if(! $ifEvent) {
                    /**
                     * Get Event by Class, Gender and Regatta Id
                     */
                    $discId = false;
                    if(!XrrEventClass::fromXrr($division->IFClassCode())) {
                        $discId = true;
                        $model->addValidationMessages("Division@IFClassID '" . $division->IFClassCode() . "' is not valid.  Division@DivisionID: " . $division->DivisionId() . ";  @IFRegattaID: " . $event->IsafRegattaId() . "; @RegattaID: " . $event->RegattaId(), 'events');
                        $result = false;
                    }
                    if(!XrrEventGender::isValidValue(XrrEventGender::fromXrr($division->Gender()))) {
                        $discId = true;
                        $model->addValidationMessages("Division@Gender '" . $division->Gender() . "' is not valid.  Division@DivisionID: " . $division->DivisionId() . ";  @IFRegattaID: " . $event->IsafRegattaId() . "; @RegattaID: " . $event->RegattaId(), 'events');
                        $result = false;
                    }

                    $ifEvent = $model->identifyEvent($recordId, $division->IFClassCode(), $division->Gender(), $division->IsDisabled());

                    if(! $discId
                        && (! $ifEvent)
                        && (! XrrDivisionType::isValidValue($divisionType))
                        ) {
                        $model->addValidationMessages("Division@DivisionId " . $division->DivisionId() . " has no available data mapping and Division@Type is missing or empty preventing it from being created.   Division@DivisionID: " . $division->DivisionId() . ";  @IFRegattaID: " . $event->IsafRegattaId() . "; @RegattaID: " . $event->RegattaId(), 'events');
                        $result = false;
                    }

                    $teamIdsInUse = array();
                    $sResults = $division->SeriesResults();
                    foreach($sResults as $sResult) {
                        if(!isset($teamIdsInUse[$sResult->TeamId()])) {
                            $teamIdsInUse[$sResult->TeamId()] = 0;
                        }
                        $teamIdsInUse[$sResult->TeamId()]++;
                    }
                    foreach($teamIdsInUse as $usedId => $usedIdCt) {
                        if($usedIdCt > 1) {
                            $model->addValidationMessages("Team@TeamID '" . $usedId . "' has been issued with multiple SeriesResult elements, please provide one final ranking per team.  Use the field Race@Stage to identify rounds.  Division@DivisionID: " . $division->DivisionId() . ";  @IFRegattaID: " . $event->IsafRegattaId() . "; @RegattaID: " . $event->RegattaId(), 'events');
                            $result = false;
                        }
                    }

                    unset($discId);
                }

                if($ifEvent) {
                    if (! $this->validateGender($model, $sailingXrr, $division, $ifEvent)) {//$this->DoGenderCheck($model, $sailingXrr, $division, $ifEvent);
                        $result = false;
                    }
                }

                $raceResults = $division->RaceResults();
                $seriesResults = $division->SeriesResults();
                $teamResults = $division->TeamRaceResults();

                foreach($raceResults as $raceResult)
                {

                    if(strlen($raceResult->RaceId()) === 0) {
                        $model->addValidationMessages("RaceResult@RaceID is missing.  Division@DivisionID: " . $division->DivisionId() . ";  @IFRegattaID: " . $event->IsafRegattaId() . "; @RegattaID: " . $event->RegattaId(), 'events');
                        $result = false;
                    } else {
                        if(!in_array($raceResult->RaceId(), $raceIds)) {
                            $model->addValidationMessages("RaceResult@RaceID " . $raceResult->RaceId() . " is not present in the Race entities.  Division@DivisionID: " . $division->DivisionId() . ";  @IFRegattaID: " . $event->IsafRegattaId() . "; @RegattaID: " . $event->RegattaId(), 'events');
                            $result = false;
                        }
                        $raceResultIds[] = $raceResult->RaceId();
                    }

                    if(strlen($raceResult->TeamId()) === 0) {
                        $model->addValidationMessages("RaceResult@TeamID is missing.  RaceResult@RaceID: " . $raceResult->RaceId() . "; Division@DivisionID: " . $division->DivisionId() . ";  @IFRegattaID: " . $event->IsafRegattaId() . "; @RegattaID: " . $event->RegattaId(), 'events');
                        $result = false;
                    } else {
                        if(!in_array($raceResult->TeamId(), $model->getTeamSourceIds())) {
                            $model->addValidationMessages("RaceResult@TeamID " . $raceResult->TeamId() . " is not present in the Team entities.  RaceResult@RaceID: " . $raceResult->RaceId() . "; Division@DivisionID: " . $division->DivisionId() . ";  @IFRegattaID: " . $event->IsafRegattaId() . "; @RegattaID: " . $event->RegattaId(), 'events');
                            $result = false;
                        }
                    }

                    if($divisionType === XrrDivisionType::TEAM_RACE)
                    {
                        if(strlen($raceResult->BoatId()) === 0) {
                            $model->addValidationMessages("RaceResult@BoatID is missing.  RaceResult@RaceID: " . $raceResult->RaceId() . "; Division@DivisionID: " . $division->DivisionId() . "; Regatta@RegattaID: " . $event->RegattaId(), 'events');
                            $result = false;
                        } else {
                            if(!in_array($raceResult->BoatId(), $model->getBoatSourceIds())) {
                                $model->addValidationMessages("RaceResult@BoatID " . $raceResult->BoatId() . " is not present in the Boat entities.  RaceResult@RaceID: " . $raceResult->RaceId() . "; Division@DivisionID: " . $division->DivisionId() . "; Regatta@RegattaID: " . $event->RegattaId(), 'events');
                                $result = false;
                            }
                        }
                    } else {
                        if(strlen($raceResult->BoatId()) > 0 && !in_array($result->BoatId(), $model->getBoatSourceIds())) {
                            $model->addValidationMessages("RaceResult@BoatID " . $raceResult->BoatId() . " is not present in the Boat entities.  RaceResult@RaceID: " . $raceResult->RaceId() . "; Division@DivisionID: " . $division->DivisionId() . ";  @IFRegattaID: " . $event->IsafRegattaId() . "; @RegattaID: " . $event->RegattaId(), 'events');
                            $result = false;
                        }
                        $teams = $model->getTeams();
                        if(isset($teams[$raceResult->TeamId()])) {
                            if(!in_array($teams[$raceResult->TeamId()]->BoatId(), $model->getBoatSourceIds())) {
                                $model->addValidationMessages("Team@BoatID '" . $teams[$raceResult->TeamId()]->BoatId() . "' is not present in the Boat entities.  Division@DivisionId: " . $division->DivisionId() . "; RaceResult@RaceID: " . $raceResult->RaceId() . "; IFEventID: " . $event->RegattaId(), 'events');
                                $result = false;
                            }
                        }
                    }

                    if(strlen($raceResult->ScoreCode()) > 0 && (! XrrBoatStatus::isValidValue($raceResult->ScoreCode(), false))) {
                        $model->addValidationMessages("RaceResult@ScoreCode '" . $raceResult->ScoreCode() . "' is not valid.  RaceResult@RaceID: " . $raceResult->RaceId() . "; Division@DivisionID: " . $division->DivisionId() . ";  @IFRegattaID: " . $event->IsafRegattaId() . "; @RegattaID: " . $event->RegattaId(), 'events');
                        $result = false;
                    }

                    if(!is_numeric($raceResult->RacePoints())) {
                        $model->addValidationMessages("RaceResult@RacePoints '" . $raceResult->RacePoints() . "' is not a numeric value. RaceResult@RaceID: " . $raceResult->RaceId() . "; Division@DivisionID: " . $division->DivisionId() . ";  @IFRegattaID: " . $event->IsafRegattaId() . "; @RegattaID: " . $event->RegattaId(), 'events');
                        $result = false;
                    }

                    if(!is_numeric($raceResult->RaceRank())) {
                        if(is_numeric($raceResult->RacePoints())) {
                            /**
                             * TODO: It doesn't really do anything
                             */
                            $raceResult->RaceRank($raceResult->RacePoints());
                        } else {
                            $model->addValidationMessages("RaceResult@RaceRank '" . $raceResult->RaceRank() . "' is not a numeric value. RaceResult@RaceID: " . $raceResult->RaceId() . "; Division@DivisionID: " . $division->DivisionId() . ";  @IFRegattaID: " . $event->IsafRegattaId() . "; @RegattaID: " . $event->RegattaId(), 'events');
                            $result = false;
                        }
                    }

                    $rResCrew = $raceResult->Crew();
                    foreach($rResCrew as $cMemb) {
                        if(!in_array($cMemb->PersonId(), $model->getSailorSourceIds())) {
                            $model->addValidationMessages("RaceRank->Crew@PersonID '" . $cMemb->PersonId() . "' is not present in the Person entities.  RaceResult@RaceID: " . $raceResult->RaceId() . "; Division@DivisionID: " . $division->DivisionId() . ";  @IFRegattaID: " . $event->IsafRegattaId() . "; @RegattaID: " . $event->RegattaId(), 'events');
                            $result = false;
                        }
                    }

                    $roundings = $raceResult->MarkRoundings();
                    foreach($roundings as $rounding) {

                        if(!in_array($rounding->SeqId(), $mkSeqMap[$raceResult->RaceId()])) {
                            $model->addValidationMessages("MarkRounding SeqId '" . $rounding->SeqId() . "' is not present in the MarkSequence list for the Course set-up. RaceResult@RaceID: " . $raceResult->RaceId() . "; Division@DivisionID: " . $division->DivisionId() . ";  @IFRegattaID: " . $event->IsafRegattaId() . "; @RegattaID: " . $event->RegattaId(), 'events');
                            $result = false;
                        }
                    }
                }

                $ranks = array();
                foreach($seriesResults as $sResult) {
                    if(strlen($sResult->TeamId()) === 0) {
                        $model->addValidationMessages("SeriesResult@TeamID is missing.  Division@DivisionID: " . $division->DivisionId() . ";  @IFRegattaID: " . $event->IsafRegattaId() . "; @RegattaID: " . $event->RegattaId(), 'events');
                        $result = false;
                    } else {
                        if(!in_array($sResult->TeamId(), $model->getTeamSourceIds())) {
                            $model->addValidationMessages("SeriesResult@TeamID " . $sResult->TeamId() . " is not present in the Team entities.  Division@DivisionID: " . $division->DivisionId() . ";  @IFRegattaID: " . $event->IsafRegattaId() . "; @RegattaID: " . $event->RegattaId(), 'events');
                            $result = false;
                        }
                    }

                    if(!$sResult->Rank()) {
                        $model->addValidationMessages("SeriesResult@Rank is missing or invalid.  Division@DivisionID: " . $division->DivisionId() . ";  @IFRegattaID: " . $event->IsafRegattaId() . "; @RegattaID: " . $event->RegattaId(), 'events');
                        $result = false;
                    } else {
                        if(!isset($ranks[$sResult->Rank()])) {
                            $ranks[$sResult->Rank()] = ($sResult->Tied() ? true : false);
                        } else {
                            if($ranks[$sResult->Rank()] === false || $sResult->Tied() === false) {
                                $model->addValidationMessages("SeriesResult@Rank '" . $sResult->Rank() . "' has been assigned to more than one result element however SeriesResult@Tied is not set to true.  Division@DivisionID: " . $division->DivisionId() . ";  @IFRegattaID: " . $event->IsafRegattaId() . "; @RegattaID: " . $event->RegattaId(), 'events');
                                $result = false;
                            }
                        }
                    }
                }

                if(count($teamResults) > 0) {
                    $division->DivisionType(XrrDivisionType::TEAM_RACE);
                }
                foreach($teamResults as $tResult) {
                    if(strlen($tResult->TeamId()) === 0) {
                        $model->addValidationMessages("TRResult@TeamID is missing.  Division@DivisionID: " . $division->DivisionId() . ";  @IFRegattaID: " . $event->IsafRegattaId() . "; @RegattaID: " . $event->RegattaId(), 'events');
                        $result = false;
                    } else {
                        if(!in_array($tResult->TeamId(), $model->getTeamSourceIds())) {
                            $model->addValidationMessages("TRResult@TeamID " . $tResult->TeamId() . " is not present in the Team entities.  Division@DivisionID: " . $division->DivisionId() . ";  @IFRegattaID: " . $event->IsafRegattaId() . "; @RegattaID: " . $event->RegattaId(), 'events');
                            $result = false;
                        }
                    }

                    if(strlen($tResult->RaceId()) === 0) {
                        $model->addValidationMessages("TRResult@RaceID is missing.  Division@DivisionID: " . $division->DivisionId() . ";  @IFRegattaID: " . $event->IsafRegattaId() . "; @RegattaID: " . $event->RegattaId(), 'events');
                        $result = false;
                    } else {
                        if(!in_array($tResult->RaceId(), $raceIds)) {
                            $model->addValidationMessages("TRResult@RaceID " . $tResult->RaceId() . " is not present in the Race entities.  Division@DivisionID: " . $division->DivisionId() . ";  @IFRegattaID: " . $event->IsafRegattaId() . "; @RegattaID: " . $event->RegattaId(), 'events');
                            $result = false;
                        }
                    }
                }
            }

        } else {
            $model->addValidationMessages("Specified Regatta@IFRegattaID '" . $event->IsafRegattaId() . "' is not valid", 'events');
            $result = false;
        }
        return $result;
    }

    /**
     * @param XrrImportModelInterface $model
     * @param XrrSailingXrr $sailingXrr
     * @param $division
     * @param $ifEvent
     * @return bool
     */
    protected function validateGender(XrrImportModelInterface &$model, XrrSailingXrr &$sailingXrr, $division, $ifEvent)
    {
        $result = true;

        $genderMismatches = array();
        $dGen = XrrEventGender::fromXrr($division->Gender());

        if(in_array($dGen, array(XrrEventGender::FEMALE, XrrEventGender::MALE))) {

            if (XrrEventGender::fromXrr($division->Gender()) == XrrEventGender::fromXrr($ifEvent['EvntTypeName'])) {

                $xrrPersons = $sailingXrr->Persons();
                $persons = array();
                $ifIds = array();
                foreach ($xrrPersons as $person) {
                    $persons[$person->PersonId()] = $person;
                    $ifIds[] = $person->ISAFID();
                }
                unset($xrrPersons);

                $ifPersons = $model->getMembers($ifIds, true);

                $xrrTeams = $sailingXrr->Teams();
                $teams = array();
                foreach ($xrrTeams as $team) {

                    $teams[$team->TeamId()] = $team;
                }
                unset($xrrTeams);

                $results = $division->RaceResults();

                foreach ($results as $raceResult) {

                    $crew = $raceResult->Crew();
                    foreach ($crew as $crewPers) {
                        $ifMemb = null;

                        if (isset($persons[$crewPers->PersonId()]) && $persons[$crewPers->PersonId()]->ISAFID() && isset($ifPersons[$persons[$crewPers->PersonId()]->ISAFID()])) {
                            $ifMemb = $ifPersons[$persons[$crewPers->PersonId()]->ISAFID()];
                        }

                        if ($ifMemb) {

                            if (XrrPersonGender::fromXrr($ifMemb['MembGender']) !== XrrPersonGender::fromXrr($dGen)) {
                                if (!in_array($ifMemb['MembId'], $genderMismatches)) {
                                    $model->addValidationMessages("Crew member gender does not match the Division allocation '" . $division->Gender() . "'. Division@DivisionID: " . $division->DivisionId() . "; RaceResult@RaceID: " . $raceResult->RaceId() . "; Person@PersonID: " . $crewPers->PersonId() . "; Person@IFPersonID: " . $ifMemb['MembLogin'], 'events');
                                    $genderMismatches[] = $ifMemb->MembId;
                                    $result = false;
                                }
                            }
                        }
                    }
                }

                $results = array_merge($results, $division->SeriesResults());
                foreach ($results as $resultTeamRef) {

                    $team = $teams[$resultTeamRef->TeamId()];
                    if ($team) {
                        $crew = $team->Crew();
                        foreach ($crew as $crewPers) {
                            $ifMemb = null;

                            if (isset($persons[$crewPers->PersonId()]) && $persons[$crewPers->PersonId()]->ISAFID() && isset($ifPersons[$persons[$crewPers->PersonId()]->ISAFID()])) {
                                $ifMemb = $ifPersons[$persons[$crewPers->PersonId()]->ISAFID()];
                            }

                            if ($ifMemb) {
                                if (XrrPersonGender::fromXrr($ifMemb['MembGender']) !== XrrPersonGender::fromXrr($dGen)) {

                                    if (!in_array($ifMemb['MembId'], $genderMismatches)) {
                                        $raceError = "";
                                        if ($resultTeamRef instanceof XrrRaceResult) {
                                            $raceError = " RaceResult@RaceId: " . $resultTeamRef->RaceId() . ";";
                                        }
                                        $model->addValidationMessages("Crew member gender does not match the Division allocation '" . $division->Gender() . "'. Division@DivisionID: " . $division->DivisionId() . ";" . $raceError . " Person@PersonID: " . $crewPers->PersonId() . "; Person@IFPersonID: " . $ifMemb['MembLogin'], 'events');
                                        $genderMismatches[] = $ifMemb['MembId'];
                                        $result = false;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                $model->addValidationMessages("Specified Division@Gender " . $division->Gender() ." is not match the registered division. Division@DivisionID: " . $division->DivisionId(), 'events');
                $result = false;
            }
        }
        return $result;
    }
}
