<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace Service\xrr;
use Model\xrr\XrrImportModelInterface;

/**
 * Interface XrrImportInterface
 * @package Service\xrr
 */
interface XrrImportInterface
{

    /**
     * @return string
     */
    public function getVersion();

    /**
     * @param $app
     * @return XrrImportModelInterface
     */
    public function xrrImportModel($app);

    /**
     * @param XrrImportModelInterface $model
     * @param bool $doPersistence
     * @param bool $addToApprovalQueue
     * @return bool
     */
    public function processImport(XrrImportModelInterface &$model, $doPersistence = false, $addToApprovalQueue = false);


}

