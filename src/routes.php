<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

use Controller\IndexController;
use Symfony\Component\HttpFoundation\Request;

/*
 * Mount available controllers by routes
 */
$app->mount( '/', new Controller\IndexController() );
$app->mount( '/token', new Controller\TokenController() );
$app->mount( '/xrr', new Controller\XrrController() );

//Default error handler
$app->error(function (\Exception $e, Request $request, $code) use ($app) {

    if ($app['debug']) {
        return;
    }

    return $app['output.error']($code);
});
