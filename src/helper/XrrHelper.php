<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Helper;

/**
 * Class XrrHelper
 * @package Helper
 */
class XrrHelper {

    public static function populateCalendarData()
    {
        $jscr_date_info = array();
        $jscr_date_info['monthnames'] = "January,Febuary,March,April,May,June,July,August,September,October,November,December";;
        $jscr_date_info['monthnames_short'] = "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec";
        $jscr_date_info['daynames'] = "Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday";
        $jscr_date_info['daynames_short'] = "Sun,Mon,Tues,Weds,Thurs,Fri,Sat";
        $jscr_date_info['daynames_min'] = "Su,Mo,Tu,We,Th,Fr,Sa";
        $date_info = $jscr_date_info;

        foreach ($jscr_date_info as $i => $dinfo) {
            $dinfo = explode(",", $dinfo);
            $date_info[$i] = $dinfo;
            foreach ($dinfo as $j => $itemname) {
                $itemname = trim($itemname);
                if (strlen($itemname) < 1) {
                    unset($dinfo[$j]);
                } else {
                    $dinfo[$j] = "'" . $itemname . "'";
                }
            }
            $jscr_date_info[$i] = "[" . implode(",", $dinfo) . "]";
        }
        return $date_info;
    }


    public static function xrrDate($timestamp)
    {
        return gmdate("Y-m-d", $timestamp);
    }

    public static function xrrTime($timestamp)
    {
        return gmdate("H:i:s", $timestamp);
    }

    public static function xrrDateTime($timestamp)
    {
        return implode("T", array(self::XrrDate($timestamp), self::XrrTime($timestamp)));
    }

    public static function boolIn($value)
    {
        $ret = $value;
        if (is_numeric($ret)) {
            if (!in_array($ret, array(0, 1))) {
                $ret = 0;
            }
            $ret = strval($ret);
        }
        if (is_bool($ret)) {
            $ret = ($ret ? "true" : "false");
        }
        if (!is_string($ret)) {
            $ret = ($ret ? "true" : "false");
        }
        if (is_string($ret) && !in_array($ret, array("1", "true", "0", "false"))) {
            $ret = "false";
        }
        return strtolower($ret);
    }

    public static function boolOut($value)
    {
        $ret = self::BoolIn($value);
        $ret = in_array($ret, array("true", "1")) ? true : false;
        return $ret;
    }

    public static function legacyDateParse($value)
    {
        $ret = null;
        if (strpos($value, ".") !== false) {
            $value = explode(".", $value);
        } else if (strpos($value, "\\") !== false) {
            $value = explode("\\", $value);
        } else if (strpos($value, "/") !== false) {
            $value = explode("/", $value);
        } else if (strpos($value, "-") !== false) {
            $value = explode("-", $value);
        } else if (is_numeric($value)) {
            $y = "";
            $m = "";
            $d = "";

            for ($sidx = 0; $sidx < strlen($value); $sidx++) {
                if (strlen($y) < 4) {
                    $y .= $value{$sidx};
                } else if (strlen($m) < 2) {
                    $m .= $value{$sidx};
                } else if (strlen($d) < 2) {
                    $d .= $value{$sidx};
                }
            }
            $value = array($y, $m, $d);
        }
        if (count($value) === 3) {
            if (strlen($value[2]) === 4) {
                $value = array_reverse($value);
            }

            if (strlen($value[2]) === 1) {
                $value[2] = "0" . $value[2];
            }
            if (is_numeric($value[1]) && strlen($value[1]) === 1) {
                $value[1] = "0" . $value[1];
            } else if (!is_numeric($value[1])) {
                $found = false;
                $calendar_data = self::populateCalendarData();
                foreach ($calendar_data['monthnames'] as $midx => $month) {
                    if (strtoupper($value[1]) === strtoupper($month)) {
                        $value[1] = $midx + 1;
                        $found = true;
                        break;
                    }
                }
                if (!$found) {
                    foreach ($calendar_data['monthnames_short'] as $midx => $month) {
                        if (strtoupper($value[1]) === strtoupper($month)) {
                            $value[1] = $midx + 1;
                            $found = true;
                            break;
                        }
                    }
                }
                if (is_numeric($value[1]) && strlen($value[1]) === 1) {
                    $value[1] = "0" . $value[1];
                }
            }

            if (strlen($value[0]) === 4 && strlen($value[1]) === 2 && strlen($value[2]) === 2) {
                $ret = implode("-", $value);
            }
        }
        return $ret;
    }

    public static function legacyTimeParse($value)
    {
        $ret = null;
        if (strpos($value, ":")) {
            $value = explode(":", $value);
        } else if (is_numeric($value)) {
            $h = "";
            $m = "";
            $s = "";
            for ($sidx = 0; $sidx < strlen($value); $sidx++) {
                if (strlen($h) < 2) {
                    $h .= $value{$sidx};
                } else if (strlen($m) < 2) {
                    $m .= $value{$sidx};
                } else if (strlen($s) < 2) {
                    $s .= $value{$sidx};
                } else {
                    $sidx = strlen($value) + 1;
                }
            }
            $value = array($h, $m, $s);
        }
        if (is_array($value)) {
            while (count($value) < 3) {
                $value[] = "00";
            }

            $ret = implode(":", $value);
        }
        return $ret;
    }


    public static function serialise(&$something)
    {
        $something = base64_encode(serialize($something));
        return $something;
    }


    public static function unSerialise(&$something)
    {
        if(strpos($something, "{") === false)
        {
            $something = base64_decode($something);
        }
        $something = unserialize($something);
        return $something;
    }

}

