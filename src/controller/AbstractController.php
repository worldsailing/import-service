<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Controller;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Provider\OutputProvider;

/**
 * Class AbstractController
 * @package Controller
 */
Abstract class AbstractController implements ControllerProviderInterface
{
    /**
     * @param Application $app
     * @return null
     */
    public function connect(Application $app)
    {
        $app->register(new OutputProvider, []);
        return null;
    }
}

