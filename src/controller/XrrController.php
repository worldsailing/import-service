<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Controller;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use worldsailing\Api\response\xrr\XrrImportResultSet;
use worldsailing\Common\Exception\WsServiceException;
use worldsailing\SimpleBiog\provider\SimpleBiogProvider;
use worldsailing\SoticMembers\provider\SoticMembersProvider;
use Model\xrr\XrrConfigLoader;
use Provider\XrrServiceProvider;


/**
 * Class XrrController
 * @package Controller
 */
class XrrController extends AbstractController  implements ControllerProviderInterface
{
    /**
     * @param Application $app
     * @return mixed
     */
    public function connect(Application $app )
    {
        parent::connect($app);
        $controller = $app['controllers_factory'];
        $controller->get("/", array( $this, 'index' ) )->bind( 'xrrimport' );

        $app->register(new SimpleBiogProvider(), []);

        $app->register(new SoticMembersProvider(), []);

        return $controller;
    }

    /**
     * @param Application $app
     * @return mixed
     */
    public function index(Application $app )
    {

        $xrr = file_get_contents(PATH_WEB . '/data/miami.xrr');

        try {
            $config = (new XrrConfigLoader())->setConfigSet($app, $xrr);

            $app->register(new XrrServiceProvider(), [
                'xrr.config' => $config
            ]);
            /** @var \Model\xrr\XrrImportModelInterface $importModel */
            $importModel = $app['service.xrr.import']->xrrImportModel($app);
            $importModel->loadData($xrr);

            $report = $app['service.xrr.import']->processImport($importModel);

            $response = new XrrImportResultSet('result', $report);

        } catch (WsServiceException $e) {
            //response by error
            //return $app['output.entity']( $e->__map(), $e->getCode());

            return $app['output.dump']($e->__map());
        } // catch (\Exception $e) {  //Comment out it for developing, because it hides notices
            //reponse by error
            //return $app['output.error']($e->getCode(), $e->getMessage(), $e->getTraceAsString());
        //}

        //response by entity
        //return $app['output.entity']($response->map());

        return $app['output.dump']($response->map());
    }
}
